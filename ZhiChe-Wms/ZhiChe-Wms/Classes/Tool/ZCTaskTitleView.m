//
//  ZCTaskTitleView.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/15.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCTaskTitleView.h"

@interface ZCTaskTitleView ()
@property (nonatomic, strong) UIButton *preButton;
@end

@implementation ZCTaskTitleView
#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = ZCColor(0xffffff, 1);
    }
    return self;
}

#pragma mark - 属性方法
- (void)setTitleArray:(NSArray *)titleArray {
    _titleArray = titleArray;
    NSInteger taskCount = titleArray.count;
    for (int i = 0; i < taskCount; i++) {
        UIButton *taskButton = [[UIButton alloc] initWithFrame:CGRectMake(SCREENWIDTH/taskCount*i, 0, SCREENWIDTH/taskCount, space(76))];
        [taskButton setTitle:titleArray[i] forState:UIControlStateNormal];
        [taskButton setTitleColor:ZCColor(0xff8213, 1) forState:UIControlStateSelected];
        [taskButton setTitleColor:ZCColor(0x000000, 0.87) forState:UIControlStateNormal];
        taskButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(26)];
        taskButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        taskButton.tag = i*100;
        [taskButton addTarget:self action:@selector(taskButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:taskButton];
        
        UIView *taskView = [[UIView alloc] initWithFrame:CGRectMake(SCREENWIDTH/taskCount*i, space(76), SCREENWIDTH/taskCount, space(4))];
        taskView.backgroundColor = ZCColor(0xff8213, 1);
        taskView.tag = i*100 + 1;
        taskView.hidden = YES;
        [self addSubview:taskView];
        
        if (i == 0) {
            taskButton.selected = YES;
            taskView.hidden = NO;
            self.preButton = taskButton;
        }
    }
}

#pragma mark - 按钮点击
- (void)taskButtonClick:(UIButton *)sender {
    NSInteger index = self.preButton.tag;
    if (sender.tag != index) {
        self.preButton.selected = NO;
        [self viewWithTag:index+1].hidden = YES;
        self.preButton = sender;
        sender.selected = YES;
        [self viewWithTag:sender.tag+1].hidden = NO;
    }
    if ([self.delegate respondsToSelector:@selector(taskTitleViewButttonWithTitle:)]) {
        [self.delegate taskTitleViewButttonWithTitle:sender.titleLabel.text];
    }
    
}

@end
