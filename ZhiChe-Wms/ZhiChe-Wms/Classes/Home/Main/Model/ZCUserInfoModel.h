//
//  ZCUserInfoModel.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/14.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

//用户权限
@interface ZCUserInfoPermissionModel : NSObject

@property (nonatomic, copy) NSString *permissionId;     //id
@property (nonatomic, copy) NSString *identity;         //标识(PC,APP)
@property (nonatomic, copy) NSString *pid;              //所属上级
@property (nonatomic, copy) NSString *name;             //名称
@property (nonatomic, copy) NSString *type;             //类型
@property (nonatomic, copy) NSString *permissionValue;  //权限值
@property (nonatomic, copy) NSString *uri;              //路径
@property (nonatomic, copy) NSString *icon;             //图标
@property (nonatomic, copy) NSString *orders;           //排序
@property (nonatomic, copy) NSString *status;           //状态(1:正常，0:失效)
@property (nonatomic, copy) NSString *gmtCreate;        //创建时间

@end

//仓库信息
@interface ZCUserInfoStoreModel : NSObject

@property (nonatomic, copy) NSString *storeId;      //id
@property (nonatomic, copy) NSString *code;         //发车点编码
@property (nonatomic, copy) NSString *name;         //发车点名称
@property (nonatomic, copy) NSString *province;     //所在省份
@property (nonatomic, copy) NSString *city;         //所在城市
@property (nonatomic, copy) NSString *county;       //所在区县
@property (nonatomic, copy) NSString *address;      //详细地址
@property (nonatomic, copy) NSString *linkMan;      //联系人
@property (nonatomic, copy) NSString *linkPhone;    //联系电话
@property (nonatomic, copy) NSString *maxStorage;   //最大存储量
@property (nonatomic, copy) NSString *status;       //状态(10:正常，20:失效)
@property (nonatomic, copy) NSString *remark;       //备注
@property (nonatomic, copy) NSString *userCreate;   //创建人
@property (nonatomic, copy) NSString *userModified; //修改人
@property (nonatomic, copy) NSString *gmtCreate;    //创建时间
@property (nonatomic, copy) NSString *gmtModified;  //修改时间

@end


@interface ZCOriginLocationModel : NSObject
@property (nonatomic, copy) NSString *originId;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *county;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *linkMan;
@property (nonatomic, copy) NSString *linkPhone;
@property (nonatomic, copy) NSString *isSeek;
@property (nonatomic, assign) BOOL isSeekChecked;
@property (nonatomic, copy) NSString *isMove;
@property (nonatomic, assign) BOOL isMoveChecked;
@property (nonatomic, copy) NSString *isPick;
@property (nonatomic, assign) BOOL isPickChecked;
@property (nonatomic, copy) NSString *isShip;
@property (nonatomic, assign) BOOL isShipChecked;
@property (nonatomic, copy) NSString *status;
@property (nonatomic, copy) NSString *remark;
@property (nonatomic, copy) NSString *userCreate;
@property (nonatomic, copy) NSString *userModified;
@property (nonatomic, copy) NSString *gmtCreate;
@property (nonatomic, copy) NSString *gmtModified;
@property (nonatomic, assign) BOOL own;

@end

@interface ZCUserInfoModel : NSObject

@property (nonatomic, copy) NSString *Id;                 //id
@property (nonatomic, copy) NSString *code;               //编码
@property (nonatomic, copy) NSString *name;               //姓名
@property (nonatomic, copy) NSString *mobile;             //手机号
@property (nonatomic, copy) NSString *email;              //电子邮箱
@property (nonatomic, copy) NSString *gender;             //性别
@property (nonatomic, copy) NSString *idNumber;           //身份证号
@property (nonatomic, copy) NSString *birthday;           //出生日期
@property (nonatomic, copy) NSString *department;         //所属部门
@property (nonatomic, copy) NSString *leader;             //上级领导
@property (nonatomic, copy) NSString *licenseType;        //驾照类型(A1,A2,B1,B2,C)
@property (nonatomic, copy) NSString *drivingLicense;     //驾照证号
@property (nonatomic, copy) NSString *firstLicenseDate;   //初次领证日期
@property (nonatomic, copy) NSString *accountId;          //账号Id
@property (nonatomic, copy) NSString *status;             //状态(1:正常,0:失效)
@property (nonatomic, copy) NSString *remark;             //备注
@property (nonatomic, copy) NSString *createUser;         //创建人
@property (nonatomic, copy) NSString *modifiedUser;       //修改人
@property (nonatomic, copy) NSString *gmtCreate;          //创建时间
@property (nonatomic, copy) NSString *gmtModified;        //修改时间
@property (nonatomic, strong) NSArray<ZCUserInfoPermissionModel *> *permissions;   //用户权限数组
@property (nonatomic, strong) NSArray<ZCUserInfoStoreModel *> *storehouses;        //仓库数组
@property (nonatomic, strong) NSArray<ZCOriginLocationModel *> *opDeliveryPoints;  //发车点数组
@property (nonatomic, copy) NSString *invalidLicenseDate;  //驾驶证有效期
@property (nonatomic, copy) NSString *invalidInsuranceDate;//意外险有效期


@end

@interface ZCAppVersionModel : NSObject

@property (nonatomic, copy) NSString *Id;
@property (nonatomic, copy) NSString *isForce;
@property (nonatomic, copy) NSString *releaseNote;
@property (nonatomic, copy) NSString *releaseNoteCN;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *versionCode;
@property (nonatomic, copy) NSString *version;
@property (nonatomic, copy) NSString *ext1;
@property (nonatomic, copy) NSString *ext2;
@property (nonatomic, copy) NSString *ext3;
@property (nonatomic, copy) NSString *gmtCreate;
@property (nonatomic, copy) NSString *gmtUpdate;

@end
