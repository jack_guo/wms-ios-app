//
//  ZCHomeView.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/14.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCHomeView.h"
#import "ZCHomeCollectionViewCell.h"


static NSString *homeCell = @"ZCHomeCollectionViewCell";

@interface ZCHomeView ()<UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) UIView *titleView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *searchView;
@property (nonatomic, strong) UIImageView *searchImageView;
@property (nonatomic, strong) UIButton *searchButton;
@property (nonatomic, strong) UIImageView *cancelImageView;
@property (nonatomic, strong) UIButton *qrCodeButton;

@property (nonatomic, strong) UIView *listView;
@property (nonatomic, strong) UICollectionView *collectionView;
@end

@implementation ZCHomeView
#pragma mark - 懒加载
- (UIView *)titleView {
    if (!_titleView) {
        _titleView = [[UIView alloc] init];
        _titleView.backgroundColor = ZCColor(0xff8213, 1);
    }
    return _titleView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"康舶司";
        _titleLabel.textColor = ZCColor(0xffffff, 1);
        _titleLabel.font = [UIFont systemFontOfSize:FontSize(36)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (UIView *)searchView {
    if (!_searchView) {
        _searchView = [[UIView alloc] init];
        _searchView.backgroundColor = [UIColor whiteColor];
        _searchView.layer.masksToBounds = YES;
        _searchView.layer.cornerRadius = space(31);
    }
    return _searchView;
}

- (UIImageView *)searchImageView {
    if (!_searchImageView) {
        _searchImageView = [[UIImageView alloc] init];
        _searchImageView.image = [UIImage imageNamed:@"home_search"];
    }
    return _searchImageView;
}

- (UIButton *)searchButton {
    if (!_searchButton) {
        _searchButton = [[UIButton alloc] init];
        [_searchButton setTitle:@"请输入订单号/车架号进行查询" forState:UIControlStateNormal];
        [_searchButton setTitleColor:ZCColor(0x000000, 0.54) forState:UIControlStateNormal];
        _searchButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(26)];
        [_searchButton addTarget:self action:@selector(searchButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchButton;
}

- (UIImageView *)cancelImageView {
    if (!_cancelImageView) {
        _cancelImageView = [[UIImageView alloc] init];
        _cancelImageView.image = [UIImage imageNamed:@"home_clear"];
    }
    return _cancelImageView;
}

- (UIButton *)qrCodeButton {
    if (!_qrCodeButton) {
        _qrCodeButton = [[UIButton alloc] init];
        [_qrCodeButton setImage:[UIImage imageNamed:@"home_qrCode"] forState:UIControlStateNormal];
        [_qrCodeButton addTarget:self action:@selector(qrCodeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _qrCodeButton;
}

- (UIView *)listView {
    if (!_listView) {
        _listView = [[UIView alloc] init];
        _listView.backgroundColor = [UIColor whiteColor];
        _listView.layer.masksToBounds = YES;
        _listView.layer.cornerRadius = space(6);
    }
    return _listView;
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 100, 100) collectionViewLayout:flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        
        [_collectionView registerClass:[ZCHomeCollectionViewCell class] forCellWithReuseIdentifier:homeCell];
    }
    return _collectionView;
}

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = ZCColor(0xf7f8fb, 1);
        [self addSubview:self.titleView];
        [self.titleView addSubview:self.titleLabel];
        [self.titleView addSubview:self.searchView];
        [self.searchView addSubview:self.searchImageView];
        [self.searchView addSubview:self.searchButton];
        [self.searchView addSubview:self.cancelImageView];
        [self.titleView addSubview:self.qrCodeButton];
        
        [self addSubview:self.listView];
        [self.listView addSubview:self.collectionView];
    }
    return self;
}

#pragma mark - 按钮点击
- (void)searchButtonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(ZCHomeViewSearchButtonClick)]) {
        [self.delegate ZCHomeViewSearchButtonClick];
    }
}

- (void)qrCodeButtonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(ZCHomeViewQRCodeButtonClick)]) {
        [self.delegate ZCHomeViewQRCodeButtonClick];
    }
}

#pragma mark - 属性方法
- (void)setPermissionArr:(NSArray *)permissionArr {
    _permissionArr = permissionArr;
    NSInteger col = 0;
    if ((permissionArr.count % 3) == 0) {
        col = permissionArr.count / 3;
    }else {
        col = permissionArr.count / 3 + 1;
    }
    CGFloat h = (ImageSize(94)+space(80)*AppScale) * col + 40 * AppScale + 20 * AppScale * (col - 1);
    if (h < SCREENHEIGHT - space(356) - space(20) - 49 - 5 * AppScale) {
        __weak typeof(self)weakSelf = self;
        [self.listView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.searchView.mas_bottom).offset(space(20));
            make.centerX.equalTo(weakSelf.mas_centerX);
            make.width.mas_equalTo(341);
            make.height.mas_equalTo(h);
        }];
    }
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewFlowLayout
//item的大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((341-20*AppScale-20*AppScale)/3, ImageSize(94)+space(80)*AppScale);
}

//垂直线上两个元素之间的距离
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10*AppScale;
}

//水平线上两个元素之间的距离
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 20*AppScale;
}

//section的内边距
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(20*AppScale, 10*AppScale, 20*AppScale, 10*AppScale);
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.permissionArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZCHomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:homeCell forIndexPath:indexPath];
    ZCUserInfoPermissionModel *permissionModel = (ZCUserInfoPermissionModel *)self.permissionArr[indexPath.item];
    cell.permissionModel = permissionModel;
    return cell;
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.delegate respondsToSelector:@selector(ZCHomeViewCollectionViewCellWithIndex:)]) {
        [self.delegate ZCHomeViewCollectionViewCellWithIndex:indexPath.item];
    }
    
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return  YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    
    [_titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.height.mas_equalTo(space(356));
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weakSelf.titleView.mas_centerX);
        make.bottom.equalTo(weakSelf.searchView.mas_top).offset(-space(40));
    }];
    
    [_searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.titleView.mas_top).offset(space(154));
        make.left.equalTo(weakSelf.titleView.mas_left).offset(space(30));
        make.right.equalTo(weakSelf.qrCodeButton.mas_left).offset(-space(30));
        make.height.mas_equalTo(space(62));
    }];
    
    [_searchImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.searchView.mas_left).offset(space(20));
        make.centerY.equalTo(weakSelf.searchView.mas_centerY);
        make.width.mas_equalTo(13);
        make.height.mas_equalTo(14);
    }];
    
    [_searchButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.searchImageView.mas_right).offset(space(10));
        make.centerY.equalTo(weakSelf.searchImageView.mas_centerY);
        make.right.equalTo(weakSelf.cancelImageView.mas_left).offset(-space(20));
    }];
    
    [_cancelImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.searchView.mas_right).offset(-space(20));
        make.centerY.equalTo(weakSelf.searchButton.mas_centerY);
        make.width.mas_equalTo(15);
        make.height.mas_equalTo(15);
    }];
    
    [_qrCodeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.titleView.mas_right).offset(-space(30));
        make.centerY.equalTo(weakSelf.searchView.mas_centerY);
        make.width.mas_equalTo(22);
        make.height.mas_equalTo(21);
    }];
    
    [_listView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.searchView.mas_bottom).offset(space(20));
//        make.left.equalTo(weakSelf.mas_left).offset(space(34));
//        make.right.equalTo(weakSelf.mas_right).offset(-space(34));
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.width.mas_equalTo(341);
        make.bottom.equalTo(weakSelf.mas_bottom).offset(-(49 + 5*AppScale));
    }];
    
    [_collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.listView.mas_top);
        make.left.equalTo(weakSelf.listView.mas_left);
        make.right.equalTo(weakSelf.listView.mas_right);
        make.bottom.equalTo(weakSelf.listView.mas_bottom);
    }];

}
@end
