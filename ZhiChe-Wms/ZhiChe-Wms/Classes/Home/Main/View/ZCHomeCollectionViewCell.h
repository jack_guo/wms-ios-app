//
//  ZCHomeCollectionViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/14.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCUserInfoModel.h"

@interface ZCHomeCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) ZCUserInfoPermissionModel *permissionModel;

@end
