//
//  ZCHomeView.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/14.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ZCHomeViewDelegate <NSObject>

- (void)ZCHomeViewCollectionViewCellWithIndex:(NSInteger)index;

- (void)ZCHomeViewSearchButtonClick;

- (void)ZCHomeViewQRCodeButtonClick;

@end


@interface ZCHomeView : UIView

@property (nonatomic, strong) NSArray *permissionArr;

@property (nonatomic, weak) id<ZCHomeViewDelegate> delegate;

@end
