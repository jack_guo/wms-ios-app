//
//  ZCHomeCollectionViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/14.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCHomeCollectionViewCell.h"


@interface ZCHomeCollectionViewCell ()

@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation ZCHomeCollectionViewCell
#pragma mark - 懒加载
- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.image = [UIImage imageNamed:@"提车任务"];
    }
    return _iconImageView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = ZCColor(0x000000, 0.87);
        _titleLabel.font = [UIFont systemFontOfSize:FontSize(26)];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"提车任务";
    }
    return _titleLabel;
}

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.iconImageView];
        [self addSubview:self.titleLabel];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setPermissionModel:(ZCUserInfoPermissionModel *)permissionModel {
    _permissionModel = permissionModel;
    _titleLabel.text = permissionModel.name;
    _iconImageView.image = [UIImage imageNamed:permissionModel.name];
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    
    [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top);
        make.centerX.equalTo(weakSelf.mas_centerX);
        make.width.mas_equalTo(ImageSize(94));
        make.height.mas_equalTo(ImageSize(94));
    }];
    
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.iconImageView.mas_bottom).offset(space(20)*AppScale);
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
    }];
}

@end
