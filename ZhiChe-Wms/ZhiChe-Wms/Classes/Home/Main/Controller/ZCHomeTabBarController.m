//
//  ZCHomeTabBarController.m
//  kangbs
//
//  Created by 高睿婕 on 2018/8/14.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCHomeTabBarController.h"
#import "ZCHomeViewController.h"
#import "ZCTaskViewController.h"
#import "ZCMineViewController.h"

@interface ZCHomeTabBarController ()

@property (nonatomic, strong) ZCHomeViewController *homeViewController;
@property (nonatomic, strong) ZCTaskViewController *taskViewController;
@property (nonatomic, strong) ZCMineViewController *mineViewController;
@end

@implementation ZCHomeTabBarController
#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    self.homeViewController = [[ZCHomeViewController alloc] init];
    [self addSubView:self.homeViewController title:@"首页" normalImage:@"home_normal" selectedImage:@"home_select"];
    
    self.taskViewController = [[ZCTaskViewController alloc] init];
    [self addSubView:self.taskViewController title:@"个人任务" normalImage:@"task_normal" selectedImage:@"task_select"];
    
    self.mineViewController = [[ZCMineViewController alloc] init];
    [self addSubView:self.mineViewController title:@"我的" normalImage:@"mine_normal" selectedImage:@"mine_select"];
    
}

#pragma mark - 自定义Item样式
- (void)addSubView:(UIViewController *)subView title:(NSString *)title normalImage:(NSString *)normalImage selectedImage:(NSString *)selectedImage {
    //设置标题
    subView.tabBarItem.title = title;
    
    //设置标题字号和颜色
    NSMutableDictionary *normalAttr = [NSMutableDictionary dictionary];
    normalAttr[NSFontAttributeName] = [UIFont systemFontOfSize:FontSize(22)];
    normalAttr[NSForegroundColorAttributeName] = ZCColor(0x000000, 0.54);
    [subView.tabBarItem setTitleTextAttributes:normalAttr forState:UIControlStateNormal];
    
    NSMutableDictionary *selectAttr = [NSMutableDictionary dictionary];
    selectAttr[NSFontAttributeName] = [UIFont systemFontOfSize:FontSize(22)];
    selectAttr[NSForegroundColorAttributeName] = ZCColor(0xff8213, 1);
    [subView.tabBarItem setTitleTextAttributes:selectAttr forState:UIControlStateSelected];
    
    //设置图片
    UIImage *normalImg = [UIImage imageNamed:normalImage];
    normalImg = [normalImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    subView.tabBarItem.image = normalImg;
    
    UIImage *selectImg = [UIImage imageNamed:selectedImage];
    selectImg = [selectImg imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    subView.tabBarItem.selectedImage = selectImg;
    
    //加导航控制器
    UINavigationController *navVC = [[UINavigationController alloc] initWithRootViewController:subView];
    
    [self addChildViewController:navVC];
    
}


@end
