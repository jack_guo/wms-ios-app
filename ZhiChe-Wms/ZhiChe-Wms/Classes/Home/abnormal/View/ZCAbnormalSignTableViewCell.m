//
//  ZCAbnormalSignTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAbnormalSignTableViewCell.h"
#import "ZCAbnormalSignView.h"



@interface ZCAbnormalSignTableViewCell ()<ZCAbnormalSignViewDelegate>

@property (nonatomic, strong) UIButton *allSignButton;
@property (nonatomic, strong) ZCAbnormalSignView *signCarView;
@property (nonatomic, strong) UIButton *cloakButton;
@property (nonatomic, strong) ZCAbnormalSignView *cloakCarView;

@end

@implementation ZCAbnormalSignTableViewCell
#pragma mark - 懒加载
- (UIButton *)allSignButton {
    if (!_allSignButton) {
        _allSignButton = [[UIButton alloc] init];
        [_allSignButton setTitle:@"全部异常" forState:UIControlStateNormal];
        [_allSignButton setTitleColor:ZCColor(0xff8213, 1) forState:UIControlStateNormal];
        _allSignButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(22)];
        _allSignButton.titleLabel.textAlignment = NSTextAlignmentRight;
        [_allSignButton addTarget:self action:@selector(allSignButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _allSignButton;
}

- (ZCAbnormalSignView *)signCarView {
    if (!_signCarView) {
        _signCarView = [[ZCAbnormalSignView alloc] initWithFrame:CGRectMake(space(130), space(90), space(492), space(492))];
        _signCarView.delegate = self;
        _signCarView.imageArray = @[@"car",@"car1",@"car2",@"car3",@"car4",@"car5",@"car6",@"car7",@"car8"];
        _signCarView.cloack = NO;
    }
    return _signCarView;
}

- (UIButton *)cloakButton {
    if (!_cloakButton) {
        _cloakButton = [[UIButton alloc] init];
        [_cloakButton setTitle:@"显示车斗" forState:UIControlStateNormal];
        [_cloakButton setTitle:@"隐藏车斗" forState:UIControlStateSelected];
        [_cloakButton setTitleColor:ZCColor(0xff8213, 1) forState:UIControlStateNormal];
        _cloakButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(22)];
        _cloakButton.titleLabel.textAlignment = NSTextAlignmentRight;
        [_cloakButton addTarget:self action:@selector(cloakButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cloakButton;
}

- (ZCAbnormalSignView *)cloakCarView {
    if (!_cloakCarView) {
        _cloakCarView = [[ZCAbnormalSignView alloc] initWithFrame:CGRectMake(space(130), space(692), space(492), space(492))];
        _cloakCarView.delegate = self;
        _cloakCarView.imageArray = @[@"",@"front",@"",@"left",@"middle",@"right",@"",@"behind",@""];
        _cloakCarView.cloack = YES;
        _cloakCarView.hidden = YES;
    }
    return _cloakCarView;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.allSignButton];
        [self addSubview:self.signCarView];
        [self addSubview:self.cloakButton];
        [self addSubview:self.cloakCarView];
    }
    return self;
}

#pragma mark - 按钮点击
- (void)allSignButtonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(abnormalSignTableViewCellAllSignButtonClick)]) {
        [self.delegate abnormalSignTableViewCellAllSignButtonClick];
    }
}

- (void)cloakButtonClick:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.cloakCarView.hidden = !self.cloakCarView.hidden;
    if ([self.delegate respondsToSelector:@selector(abnormalSignTableViewCellLoackShow:)]) {
        [self.delegate abnormalSignTableViewCellLoackShow:sender.selected];
    }
}

#pragma mark - UIAbnormalSignViewDelegate
- (void)abnormalSignView:(ZCAbnormalSignView *)signView SignButtonClickWithIndex:(NSIndexPath *)index {
    if (signView == _signCarView) {
        if ([self.delegate respondsToSelector:@selector(abnormalSignTableViewCellSignMissingButtonClickWithIndex: isloack:)]) {
            [self.delegate abnormalSignTableViewCellSignMissingButtonClickWithIndex:index isloack:NO];
        }
    }else {
        if (index.row == 1 || index.row == 3 || index.row == 4 || index.row == 5 || index.row == 7) {
            if ([self.delegate respondsToSelector:@selector(abnormalSignTableViewCellSignMissingButtonClickWithIndex: isloack:)]) {
                [self.delegate abnormalSignTableViewCellSignMissingButtonClickWithIndex:index isloack:YES];
            }
        }
    }
    
}

#pragma mark - 属性方法
- (void)setExceptTotal:(NSString *)exceptTotal {
    _exceptTotal = exceptTotal;
    if ([exceptTotal integerValue] == 0) {
        self.allSignButton.hidden = YES;
    }else {
        [self.allSignButton setTitle:[NSString stringWithFormat:@"全部异常(%@)",exceptTotal] forState:UIControlStateNormal];
        self.allSignButton.hidden = NO;
    }
}

- (void)setExceptArray:(NSArray *)exceptArray {
    _exceptArray = exceptArray;
    self.signCarView.signArray = exceptArray;
}

- (void)setCloakArray:(NSArray *)cloakArray {
    _cloakArray = cloakArray;
    self.cloakCarView.cloakArray = cloakArray;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_allSignButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(space(20));
        make.right.equalTo(weakSelf.mas_right).offset(-space(80));
//        make.width.mas_equalTo(space(300));
    }];
    
    if (Iphone5s) {
        [_signCarView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.mas_top).offset(space(90));
            make.left.equalTo(weakSelf.mas_left).offset(space(80));
            make.right.equalTo(weakSelf.mas_right).offset(-space(80));
            make.height.equalTo(weakSelf.signCarView.mas_width);
        }];
    }
    
    [_cloakButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.signCarView.mas_bottom).offset(space(20));
        make.right.equalTo(weakSelf.allSignButton.mas_right);
//        make.width.equalTo(weakSelf.allSignButton.mas_width);
    }];
    
    if (Iphone5s) {
        [_cloakCarView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.signCarView.mas_bottom).offset(space(90));
            make.left.equalTo(weakSelf.signCarView.mas_left);
            make.right.equalTo(weakSelf.signCarView.mas_right);
            make.height.equalTo(weakSelf.cloakCarView.mas_width);
        }];
    }

}


@end
