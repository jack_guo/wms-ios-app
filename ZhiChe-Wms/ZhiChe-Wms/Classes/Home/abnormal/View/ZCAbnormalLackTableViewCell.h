//
//  ZCAbnormalLackTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/24.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCAbnormalModel.h"

@protocol ZCAbnormalLackTableViewCellDelegate <NSObject>

- (void)abnormalLackTableViewCellSelectLackButton:(UIButton *)sender missingModel:(ZCAbnormalMissingModel *)missingModel;
@end

@interface ZCAbnormalLackTableViewCell : UITableViewCell

@property (nonatomic, strong) ZCAbnormalMissingModel *missingModel;
@property (nonatomic, weak) id<ZCAbnormalLackTableViewCellDelegate> delegate;

@end
