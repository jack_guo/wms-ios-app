//
//  ZCAbnormalSignView.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZCAbnormalSignView;
@protocol ZCAbnormalSignViewDelegate <NSObject>

- (void)abnormalSignView:(ZCAbnormalSignView *)signView SignButtonClickWithIndex:(NSIndexPath *)index;

@end

@interface ZCAbnormalSignView : UIView

@property (nonatomic, weak) id<ZCAbnormalSignViewDelegate> delegate;
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, assign) BOOL cloack;
@property (nonatomic, strong) NSArray *signArray;
@property (nonatomic, strong) NSArray *cloakArray;

@end
