//
//  ZCAbnormalWriteLackTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/25.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZCAbnormalModel.h"

@protocol ZCAbnormalWriteLackTableViewCellDelegate <NSObject>

- (void)abnormalWriteLackTableViewCellKeyboardWillShow:(CGRect)rect;
- (void)abnormalWriteLackTableViewCellKeyboardWillHiden;
- (void)abnormalWriteLackTableViewCellTextView:(NSString *)text missingModel:(ZCAbnormalMissingModel *)missingModel;

@end

@interface ZCAbnormalWriteLackTableViewCell : UITableViewCell

@property (nonatomic, strong) ZCAbnormalMissingModel *missingModel;
@property (nonatomic, weak) id<ZCAbnormalWriteLackTableViewCellDelegate> delegate;

@end
