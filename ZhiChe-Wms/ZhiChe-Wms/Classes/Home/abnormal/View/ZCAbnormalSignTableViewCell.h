//
//  ZCAbnormalSignTableViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZCAbnormalSignTableViewCellDelegate <NSObject>

- (void)abnormalSignTableViewCellSignMissingButtonClickWithIndex:(NSIndexPath *)index isloack:(BOOL)loack;

- (void)abnormalSignTableViewCellLoackShow:(BOOL)show;

- (void)abnormalSignTableViewCellAllSignButtonClick;

@end

@interface ZCAbnormalSignTableViewCell : UITableViewCell
@property (nonatomic, weak) id<ZCAbnormalSignTableViewCellDelegate> delegate;
@property (nonatomic, copy) NSString *exceptTotal;
@property (nonatomic, strong) NSArray *exceptArray;
@property (nonatomic, strong) NSArray *cloakArray;
@end
