//
//  ZCAbnormalSignListCollectionViewCell.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/22.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZCAbnormalSignListCollectionViewCellDelegate <NSObject>

- (void)abnormalSignListCollectionViewCellTapImageWithIndex:(NSIndexPath *)index;

- (void)abnormalSignListCollectionViewCellLongPressImageWithIndex:(NSIndexPath *)index;

@end

@interface ZCAbnormalSignListCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) id<ZCAbnormalSignListCollectionViewCellDelegate> delegate;
@property (nonatomic, strong) UIImageView *imageView;

@end
