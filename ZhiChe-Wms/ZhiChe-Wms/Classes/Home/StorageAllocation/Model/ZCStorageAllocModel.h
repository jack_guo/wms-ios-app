//
//  ZCStorageAllocModel.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/4.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCStorageAllocModel : NSObject

@property (nonatomic, copy) NSString *Id;                   //Id
@property (nonatomic, copy) NSString *headerId;             //入库单头键
@property (nonatomic, copy) NSString *seq;                  //序号
@property (nonatomic, copy) NSString *ownerId;              //货主
@property (nonatomic, copy) NSString *ownerOrderNo;         //货主单号
@property (nonatomic, copy) NSString *logLineId;            //日志行Id
@property (nonatomic, copy) NSString *lineSourceKey;        //行来源唯一键
@property (nonatomic, copy) NSString *lineSourceNo;         //行来源单据号
@property (nonatomic, copy) NSString *materielId;           //物料Id
@property (nonatomic, copy) NSString *materielCode;         //物料代码
@property (nonatomic, copy) NSString *materielName;         //物料名称
@property (nonatomic, copy) NSString *uom;                  //计量单位
@property (nonatomic, copy) NSString *expectQty;            //预计数量
@property (nonatomic, copy) NSString *expectNetWeight;      //预计净重
@property (nonatomic, copy) NSString *expectGrossWeight;    //预计毛重
@property (nonatomic, copy) NSString *expectGrossCubage;    //预计体积
@property (nonatomic, copy) NSString *expectPackedCount;    //预计件数
@property (nonatomic, copy) NSString *inboundQty;           //入库数量
@property (nonatomic, copy) NSString *inboundNetWeight;     //入库净重
@property (nonatomic, copy) NSString *inboundGrossWeight;   //入库毛重
@property (nonatomic, copy) NSString *inboundGrossCubage;   //入库体积
@property (nonatomic, copy) NSString *inboundPackedCount;   //入库数量
@property (nonatomic, copy) NSString *lotNo0;               //批号0
@property (nonatomic, copy) NSString *lotNo1;               //批号1(车架号)
@property (nonatomic, copy) NSString *lotNo2;               //批号2
@property (nonatomic, copy) NSString *lotNo3;               //批号3
@property (nonatomic, copy) NSString *lotNo4;               //批号4
@property (nonatomic, copy) NSString *lotNo5;               //批号5
@property (nonatomic, copy) NSString *lotNo6;               //批号6
@property (nonatomic, copy) NSString *lotNo7;               //批号7
@property (nonatomic, copy) NSString *lotNo8;               //批号8
@property (nonatomic, copy) NSString *lotNo9;               //批号9
@property (nonatomic, copy) NSString *status;               //状态(10：未入库，20：部分入库，30：全部入库，40：关闭入库，50：取消)
@property (nonatomic, copy) NSString *remarks;              //备注
@property (nonatomic, copy) NSString *gmtCreate;            //创建时间
@property (nonatomic, copy) NSString *gmtModified;          //修改时间
@property (nonatomic, copy) NSString *storeHouseId;         //收货仓库Id
@property (nonatomic, copy) NSString *storeHouseName;       //收货仓库名称
@property (nonatomic, copy) NSString *ownerName;            //货主名称
@property (nonatomic, copy) NSString *carrierId;            //供应商
@property (nonatomic, copy) NSString *carrierName;          //供应商名称
@property (nonatomic, copy) NSString *plateNumber;          //车船号
@property (nonatomic, copy) NSString *recvDate;             //预计入库时间
@property (nonatomic, copy) NSString *noticeNo;             //入库通知单号
@property (nonatomic, copy) NSString *locationNo;           //库位号

@end
