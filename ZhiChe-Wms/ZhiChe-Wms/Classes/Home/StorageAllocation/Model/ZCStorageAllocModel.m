//
//  ZCStorageAllocModel.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/4.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCStorageAllocModel.h"

@implementation ZCStorageAllocModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"Id" : @"id"};
}

@end
