//
//  ZCPutInStoragePlanModel.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/29.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCPutInStoragePlanModel.h"


@implementation ZCStoragePlanInfoModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"Id" : @"id"};
}

@end


@implementation ZCStoragePlanConditionModel

@end


@implementation ZCPutInStoragePlanModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"records" : [ZCStoragePlanInfoModel class]};
}

@end
