//
//  ZCPutInStoragePlanModel.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/29.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZCStoragePlanInfoModel : NSObject

@property (nonatomic, copy) NSString *Id;                   //id
@property (nonatomic, copy) NSString *headerId;             //入库通知单头键
@property (nonatomic, copy) NSString *seq;                  //序号
@property (nonatomic, copy) NSString *ownerId;              //货主
@property (nonatomic, copy) NSString *ownerOrderNo;         //货主订单号
@property (nonatomic, copy) NSString *logLineId;            //日志行Id
@property (nonatomic, copy) NSString *lineSourceKey;        //行来源唯一键
@property (nonatomic, copy) NSString *lineSourceNo;         //行来源单据号
@property (nonatomic, copy) NSString *materielId;           //物料Id
@property (nonatomic, copy) NSString *materielCode;         //物料代码
@property (nonatomic, copy) NSString *materielName;         //物料名称
@property (nonatomic, copy) NSString *uom;                  //计量单位
@property (nonatomic, copy) NSString *expectQty;            //预计数量
@property (nonatomic, copy) NSString *expectNetWeight;      //预计净重
@property (nonatomic, copy) NSString *expectGrossWeight;    //预计毛重
@property (nonatomic, copy) NSString *expectGrossCubage;    //预计体积
@property (nonatomic, copy) NSString *expectPackedCount;    //预计件数
@property (nonatomic, copy) NSString *inboundQty;           //入库数量
@property (nonatomic, copy) NSString *inboundNetWeight;     //入库净重
@property (nonatomic, copy) NSString *inboundGrossWeight;   //入库毛重
@property (nonatomic, copy) NSString *inboundGrossCubage;   //入库体积
@property (nonatomic, copy) NSString *inboundPackedCount;   //入库件数
@property (nonatomic, copy) NSString *lotNo0;               //批号0
@property (nonatomic, copy) NSString *lotNo1;               //批号1（车架号）
@property (nonatomic, copy) NSString *lotNo2;               //批号2
@property (nonatomic, copy) NSString *lotNo3;               //批号3
@property (nonatomic, copy) NSString *lotNo4;               //批号4
@property (nonatomic, copy) NSString *lotNo5;               //批号5
@property (nonatomic, copy) NSString *lotNo6;               //批号6
@property (nonatomic, copy) NSString *lotNo7;               //批号7
@property (nonatomic, copy) NSString *lotNo8;               //批号8
@property (nonatomic, copy) NSString *lotNo9;               //批号9
@property (nonatomic, copy) NSString *status;               //状态(10：未入库，20：部分入库，30：全部入库，40：关闭入库，50：取消)
@property (nonatomic, copy) NSString *remarks;              //备注
@property (nonatomic, copy) NSString *gmtCreate;            //创建时间
@property (nonatomic, copy) NSString *gmtModified;          //修改时间
@property (nonatomic, copy) NSString *noticeNo;             //入库通知单号
@property (nonatomic, copy) NSString *driverName;           //司机姓名
@property (nonatomic, copy) NSString *driverPhone;          //司机联系方式
@property (nonatomic, copy) NSString *storeHouseId;         //收货仓库id
@property (nonatomic, copy) NSString *storeHouseName;       //收货仓库名
@property (nonatomic, copy) NSString *carrierId;            //供应商Id
@property (nonatomic, copy) NSString *carrierName;          //供应商名称
@property (nonatomic, copy) NSString *inspectStatus;        //质检状态(0：未质检，10：全部合格，20：部分合格，30：全部质损)
@property (nonatomic, copy) NSString *inspectRemark;        //质检备注
@property (nonatomic, copy) NSString *createDate;           //任务创建时间
@property (nonatomic, copy) NSString *expectDate;           //预计入库时间
@property (nonatomic, copy) NSString *storeAreaId;          //仓库区域Id
@property (nonatomic, copy) NSString *storeAreaName;        //仓库区域名称
@property (nonatomic, copy) NSString *locationId;           //存储位置Id
@property (nonatomic, copy) NSString *locationCode;         //存储位置编码
@property (nonatomic, copy) NSString *ownerName;            //货主名称
@property (nonatomic, copy) NSString *plateNumber;          //车船号
@property (nonatomic, copy) NSString *recvDate;             //预计入库时间
@property (nonatomic, copy) NSString *locationNo;           //库位号
@property (nonatomic, copy) NSString *noticeLineId;         //通知单明细键
@property (nonatomic, copy) NSString *keyStatus;            //钥匙状态(10：未收取，20：已收取)
@property (nonatomic, copy) NSString *genMethod;            //资料生成方式(10：手工创建，20：道闸触发，30：APP触发)
@property (nonatomic, copy) NSString *userCreate;           //创建人
@property (nonatomic, copy) NSString *areaCode;             //库区编码
@property (nonatomic, copy) NSString *areaName;             //库区名称
@property (nonatomic, copy) NSString *locationName;         //库位名称
@property (nonatomic, copy) NSString *storeHouseCode;       //仓库编码
@property (nonatomic, copy) NSString *imgBase64;
@property (nonatomic, copy) NSString *storeDetail;          //实车位置
@property (nonatomic, copy) NSString *isCanSend;            //是否发运

@end

@interface ZCStoragePlanConditionModel : NSObject

@property (nonatomic, copy) NSString *houseId;
@property (nonatomic, copy) NSString *status;

@end

@interface ZCPutInStoragePlanModel : NSObject

@property (nonatomic, copy) NSString *offset;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *size;
@property (nonatomic, copy) NSString *pages;
@property (nonatomic, copy) NSString *current;
@property (nonatomic, copy) NSString *searchCount;
@property (nonatomic, copy) NSString *openSort;
@property (nonatomic, copy) NSString *orderByField;
@property (nonatomic, strong) NSArray<ZCStoragePlanInfoModel *> *records;
@property (nonatomic, strong) ZCStoragePlanConditionModel *condition;
@property (nonatomic, copy) NSString *offsetCurrent;
@property (nonatomic, copy) NSString *asc;

@end
