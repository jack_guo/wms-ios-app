//
//  ZCAdjustmentModel.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/6.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAdjustmentModel.h"

@implementation ZCAdjustmentInfoModel

@end

@implementation ZCAdjustmentConditionModel

@end

@implementation ZCAdjustmentModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"records" : [ZCAdjustmentInfoModel class]};
}

@end

@implementation ZCLocationInfoModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"Id" : @"id"};
}

@end

@implementation ZCLocationConditionModel

@end

@implementation ZCLocationModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"records" : [ZCLocationInfoModel class]};
}


@end
