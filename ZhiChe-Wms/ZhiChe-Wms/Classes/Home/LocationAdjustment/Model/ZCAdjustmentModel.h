//
//  ZCAdjustmentModel.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/6.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCAdjustmentInfoModel : NSObject

@property (nonatomic, copy) NSString *skuId;
@property (nonatomic, copy) NSString *ownerId;         //货主
@property (nonatomic, copy) NSString *ownerName;       //货主名称
@property (nonatomic, copy) NSString *materielId;      //物料Id
@property (nonatomic, copy) NSString *materielCode;    //物料编码
@property (nonatomic, copy) NSString *materielName;    //物料名称
@property (nonatomic, copy) NSString *uom;             //计量单位
@property (nonatomic, copy) NSString *lotNo0;          //批号0
@property (nonatomic, copy) NSString *lotNo1;          //批号1（车架号）
@property (nonatomic, copy) NSString *lotNo2;          //批号2
@property (nonatomic, copy) NSString *lotNo3;          //批号3
@property (nonatomic, copy) NSString *lotNo4;          //批号4
@property (nonatomic, copy) NSString *lotNo5;          //批号5
@property (nonatomic, copy) NSString *lotNo6;          //批号6
@property (nonatomic, copy) NSString *lotNo7;          //批号7
@property (nonatomic, copy) NSString *lotNo8;          //批号8
@property (nonatomic, copy) NSString *lotNo9;          //批号9
@property (nonatomic, copy) NSString *stockId;         //
@property (nonatomic, copy) NSString *storeHouseId;    //收货仓库Id
@property (nonatomic, copy) NSString *storeHouseName;  //收货仓库名称
@property (nonatomic, copy) NSString *storeAreaId;     //仓库区域Id
@property (nonatomic, copy) NSString *storeAreaName;   //仓库区域名称
@property (nonatomic, copy) NSString *locationId;      //库位Id
@property (nonatomic, copy) NSString *locationCode;    //库位编码
@property (nonatomic, copy) NSString *locationName;    //库位名称
@property (nonatomic, copy) NSString *qty;             //数量
@property (nonatomic, copy) NSString *netWeight;       //净重
@property (nonatomic, copy) NSString *grossWeight;     //毛重
@property (nonatomic, copy) NSString *grossCubage;     //体积
@property (nonatomic, copy) NSString *packedCount;     //件数
@property (nonatomic, copy) NSString *stockStatus;     //仓库状态
@property (nonatomic, copy) NSString *remarks;         //备注
@property (nonatomic, copy) NSString *qrCode;          //二维码
@property (nonatomic, copy) NSString *stanVehicleType; //标准车型类型
@property (nonatomic, copy) NSString *storeDetail;     //实车位置
@end

@interface ZCAdjustmentConditionModel : NSObject

@property (nonatomic, copy) NSString *lotNo1;          //模糊搜索条件
@property (nonatomic, copy) NSString *storeHouseId;    //收货仓库Id
@end

@interface ZCAdjustmentModel : NSObject

@property (nonatomic, copy) NSString *offset;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *size;
@property (nonatomic, copy) NSString *pages;
@property (nonatomic, copy) NSString *current;
@property (nonatomic, copy) NSString *searchCount;
@property (nonatomic, copy) NSString *openSort;
@property (nonatomic, copy) NSString *orderByField;

@property (nonatomic, strong) NSArray<ZCAdjustmentInfoModel *> *records;
@property (nonatomic, strong) ZCAdjustmentConditionModel *condition;

@property (nonatomic, copy) NSString *offsetCurrent;
@property (nonatomic, copy) NSString *asc;

@end


@interface ZCLocationInfoModel : NSObject

@property (nonatomic, copy) NSString *Id;                //Id
@property (nonatomic, copy) NSString *storeHouseId;      //收货仓库Id
@property (nonatomic, copy) NSString *storeHouseName;    //收货仓库名称
@property (nonatomic, copy) NSString *storeAreaId;       //库区Id
@property (nonatomic, copy) NSString *storeAreaName;     //库区名称
@property (nonatomic, copy) NSString *type;              //存储类型(10：普通储位，20：临时储位，30：虚拟库区，40：大储位)
@property (nonatomic, copy) NSString *code;              //储位编码
@property (nonatomic, copy) NSString *name;              //储位名称
@property (nonatomic, copy) NSString *maxStorage;        //最大存储量
@property (nonatomic, copy) NSString *sort;              //排序
@property (nonatomic, copy) NSString *status;            //状态(10：正常，20：失效)
@property (nonatomic, copy) NSString *remark;            //备注
@property (nonatomic, copy) NSString *userCreate;        //创建人
@property (nonatomic, copy) NSString *userModified;      //修改人
@property (nonatomic, copy) NSString *gmtCreate;         //创建时间
@property (nonatomic, copy) NSString *gmtModified;       //修改时间
@property (nonatomic, copy) NSString *storedQty;         //库存数量
@property (nonatomic, copy) NSString *usableQty;         //可用数量

@end

@interface ZCLocationConditionModel : NSObject

@property (nonatomic, copy) NSString *storeHouseId;
@property (nonatomic, copy) NSString *key;              //模糊搜索条件
@property (nonatomic, copy) NSString *status;

@end

@interface ZCLocationModel : NSObject

@property (nonatomic, copy) NSString *offset;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *size;
@property (nonatomic, copy) NSString *pages;
@property (nonatomic, copy) NSString *current;
@property (nonatomic, copy) NSString *searchCount;
@property (nonatomic, copy) NSString *openSort;
@property (nonatomic, copy) NSString *orderByField;

@property (nonatomic, strong) NSArray<ZCLocationInfoModel *> *records;
@property (nonatomic, strong) ZCLocationConditionModel *condition;

@property (nonatomic, copy) NSString *offsetCurrent;
@property (nonatomic, copy) NSString *asc;

@end
