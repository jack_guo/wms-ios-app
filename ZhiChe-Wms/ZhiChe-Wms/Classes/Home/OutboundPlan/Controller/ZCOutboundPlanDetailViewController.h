//
//  ZCOutboundPlanDetailViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/5.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCOutboundPlanDetailViewController : UIViewController

@property (nonatomic, copy) NSString *houseId;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *visitType;
@property (nonatomic, assign) BOOL fromPlan;

@end
