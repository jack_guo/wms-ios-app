//
//  ZCOutboundPlanDetailViewController.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/5.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCOutboundPlanDetailViewController.h"
#import "ZCTaskTotalNumTableViewCell.h"
#import "ZCOutboundPlanModel.h"
#import "ZCAbnormalSignViewController.h"

static NSString *totalNumCellID = @"ZCTaskTotalNumTableViewCell";

@interface ZCOutboundPlanDetailViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIView *handleView;
@property (nonatomic, strong) UIButton *abnormalButton;
@property (nonatomic, strong) UIButton *outboundButton;
@property (nonatomic, strong) ZCOutboundPlanDetailModel *detailModel;
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation ZCOutboundPlanDetailViewController
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH, space(90 * 9) + 64) style:UITableViewStyleGrouped];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = NO;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
            _tableView.scrollIndicatorInsets = _tableView.contentInset;
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
        }
        [_tableView registerClass:[ZCTaskTotalNumTableViewCell class] forCellReuseIdentifier:totalNumCellID];
    }
    return _tableView;
}

- (UIView *)handleView {
    if (!_handleView) {
        _handleView = [[UIView alloc] init];
        _handleView.backgroundColor = [UIColor whiteColor];
        _handleView.hidden = YES;
    }
    return _handleView;
}

- (UIButton *)abnormalButton {
    if (!_abnormalButton) {
        _abnormalButton = [[UIButton alloc] init];
        [_abnormalButton setTitle:@"异常登记" forState:UIControlStateNormal];
        [_abnormalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_abnormalButton setBackgroundColor:ZCColor(0xff6112, 1)];
        _abnormalButton.layer.masksToBounds = YES;
        _abnormalButton.layer.cornerRadius = space(6);
        [_abnormalButton addTarget:self action:@selector(abnormalButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _abnormalButton;
}

- (UIButton *)outboundButton {
    if (!_outboundButton) {
        _outboundButton = [[UIButton alloc] init];
        [_outboundButton setTitle:@"出库确认" forState:UIControlStateNormal];
        [_outboundButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_outboundButton setBackgroundColor:ZCColor(0xff6112, 1)];
        _outboundButton.layer.masksToBounds = YES;
        _outboundButton.layer.cornerRadius = space(6);
        [_outboundButton addTarget:self action:@selector(outboundButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        _outboundButton.timeInterval = 3;
    }
    return _outboundButton;
}

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"出库确认";
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [backButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.handleView];
    [self.handleView addSubview:self.abnormalButton];
    [self.handleView addSubview:self.outboundButton];
    [self updateViewConstraints];
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hud];
    [self loadData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.hud hideAnimated:YES];
    self.hud.mode = MBProgressHUDModeIndeterminate;
}

#pragma mark - 按钮点击
- (void)backButtonClick {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)abnormalButtonClick:(UIButton *)sender {
    ZCAbnormalSignViewController *signVC = [[ZCAbnormalSignViewController alloc] init];
    signVC.releaseId = self.detailModel.Id;
    signVC.taskType = @"51";
    signVC.visitType = @"CLICK";
    signVC.orderNoAlloc = self.detailModel.ownerOrderNo;
    signVC.vinAlloc = self.detailModel.lotNo1;
    signVC.vehicleAlloc = self.detailModel.materielId;
    signVC.storageAlloc = self.detailModel.storeHouseName;
    signVC.isCanSend = self.detailModel.isCanSend;
    [self.navigationController pushViewController:signVC animated:YES];
}

- (void)outboundButtonClick:(UIButton *)sender {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:self.detailModel.storeHouseId forKey:@"houseId"];
    [param setObject:self.detailModel.Id forKey:@"key"];

    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:OutboundCommit params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                [weakSelf.hud hideAnimated:YES];
                weakSelf.detailModel = [ZCOutboundPlanDetailModel yy_modelWithDictionary:responseObject[@"data"]];
                [weakSelf.tableView reloadData];
                weakSelf.handleView.hidden = [weakSelf.detailModel.status integerValue] == 10 ? NO : YES;
            }else {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
}

#pragma mark - 网络请求
- (void)loadData {
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setObject:self.houseId forKey:@"houseId"];
    [param setObject:self.key forKey:@"key"];
    [param setObject:self.visitType forKey:@"visitType"];
    

    [self.hud showAnimated:YES];
    __weak typeof(self)weakSelf = self;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [ZCHttpTool postWithURL:OutboundDetail params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            if ([responseObject[@"code"] integerValue] == 0) {
                [weakSelf.hud hideAnimated:YES];
                weakSelf.detailModel = [ZCOutboundPlanDetailModel yy_modelWithDictionary:responseObject[@"data"]];
                [weakSelf.tableView reloadData];
                if (!weakSelf.fromPlan) {
                    weakSelf.handleView.hidden = [weakSelf.detailModel.status integerValue] == 10 ? NO : YES;
                }
            }else {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = responseObject[@"message"];
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }
        } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = error.localizedDescription;
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }];
    });
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 9;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZCTaskTotalNumTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:totalNumCellID forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        cell.title = @"通知单号";
        cell.content = self.detailModel.noticeNo;
    }else if (indexPath.row == 1) {
        cell.title = @"订单号";
        cell.content = self.detailModel.ownerOrderNo;
    }else if (indexPath.row == 2) {
        cell.title = @"车架号";
        cell.content = self.detailModel.lotNo1;
    }else if (indexPath.row == 3) {
        cell.title = @"客户";
        cell.content = self.detailModel.ownerName;
    }else if (indexPath.row == 4) {
        cell.title = @"预计出库时间";
        cell.content = self.detailModel.recvDate;
    }else if (indexPath.row == 5) {
        cell.title = @"发货仓库";
        cell.content = self.detailModel.storeHouseName;
    }else if (indexPath.row == 6) {
        cell.title = @"承运商";
        cell.content = self.detailModel.carrierName;
    }else if (indexPath.row == 7) {
        cell.title = @"车牌号";
        cell.content = self.detailModel.plateNumber;
    }else if (indexPath.row == 8) {
        cell.title = @"状态";
        switch ([self.detailModel.status integerValue]) {
            case 10:
                cell.content = @"未出库";
                break;
            case 20:
                cell.content = @"部分出库";
                break;
            case 30:
                cell.content = @"已出库";
                break;
            case 40:
                cell.content = @"关闭出库";
                break;
            case 50:
                cell.content = @"取消";
                break;
            default:
                cell.content = @"状态未知";
                break;
        }
        cell.contentColor = ZCColor(0xff8213, 1);
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return space(90);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

#pragma mark - 布局
- (void)updateViewConstraints {
    [super updateViewConstraints];
    __weak typeof(self)weakSelf = self;
    [_handleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.tableView.mas_bottom);
        make.left.equalTo(weakSelf.view.mas_left);
        make.right.equalTo(weakSelf.view.mas_right);
        make.bottom.equalTo(weakSelf.view.mas_bottom);
    }];
    
    [_abnormalButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.handleView.mas_left).offset(space(80));
        make.right.equalTo(weakSelf.handleView.mas_centerX).offset(-space(40));
        make.top.equalTo(weakSelf.handleView.mas_top).offset(space(80));
        make.height.mas_equalTo(space(90));
    }];
    
    [_outboundButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.handleView.mas_centerX).offset(space(40));
        make.right.equalTo(weakSelf.handleView.mas_right).offset(-space(80));
        make.top.equalTo(weakSelf.abnormalButton.mas_top);
        make.height.equalTo(weakSelf.abnormalButton.mas_height);
    }];
}

@end
