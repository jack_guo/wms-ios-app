//
//  ZCOutboundPlanModel.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/5.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCOutboundPlanInfoModel : NSObject

@property (nonatomic, copy) NSString *noticeLineId;        //出库通知单头键
@property (nonatomic, copy) NSString *noticeHeadId;        //出库单头键
@property (nonatomic, copy) NSString *noticeNo;            //入库通知单号
@property (nonatomic, copy) NSString *ownerId;             //货主
@property (nonatomic, copy) NSString *ownerOrderNo;        //货主订单号
@property (nonatomic, copy) NSString *lotNo1;              //车架号
@property (nonatomic, copy) NSString *carrierName;         //供应商名称
@property (nonatomic, copy) NSString *materielId;          //物料Id
@property (nonatomic, copy) NSString *materielCode;        //物料编码
@property (nonatomic, copy) NSString *materielName;        //物料名称
@property (nonatomic, copy) NSString *driverName;          //司机姓名
@property (nonatomic, copy) NSString *plateNumber;         //车船号
@property (nonatomic, copy) NSString *driverPhone;         //司机联系方式
@property (nonatomic, copy) NSString *noticeLineStatus;
@property (nonatomic, copy) NSString *prepareLineStatus;   //
@property (nonatomic, copy) NSString *preparetor;          //备料员
@property (nonatomic, copy) NSString *storeHouseName;      //仓库名称
@property (nonatomic, copy) NSString *expectDate;          //预计出库时间
@property (nonatomic, copy) NSString *createDate;          //任务创建时间
@property (nonatomic, copy) NSString *inboundStatus;       //入库状态
@property (nonatomic, copy) NSString *inspectStatus;       //备料状态
@property (nonatomic, copy) NSString *remarks;             //备注
@property (nonatomic, copy) NSString *startTime;           //开始时间
@property (nonatomic, copy) NSString *finishTime;          //完成时间

@end

@interface ZCOutboundPlanConditionModel : NSObject

@property (nonatomic, copy) NSString *houseId;            //收货仓库id
@property (nonatomic, copy) NSString *status;             //状态

@end

@interface ZCOutboundPlanModel : NSObject

@property (nonatomic, copy) NSString *offset;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *size;
@property (nonatomic, copy) NSString *pages;
@property (nonatomic, copy) NSString *current;
@property (nonatomic, copy) NSString *searchCount;
@property (nonatomic, copy) NSString *openSort;
@property (nonatomic, copy) NSString *orderByField;
@property (nonatomic, strong) NSArray<ZCOutboundPlanInfoModel *> *records;
@property (nonatomic, strong) ZCOutboundPlanConditionModel *condition;
@property (nonatomic, copy) NSString *offsetCurrent;
@property (nonatomic, copy) NSString *asc;

@end

@interface ZCOutboundPlanDetailModel : NSObject

@property (nonatomic, copy) NSString *Id;                  //Id
@property (nonatomic, copy) NSString *headerId;            //入库通知单头键
@property (nonatomic, copy) NSString *seq;                 //序号
@property (nonatomic, copy) NSString *lineSourceKey;       //行来源唯一键
@property (nonatomic, copy) NSString *lineSourceNo;        //行来源单据号
@property (nonatomic, copy) NSString *ownerId;             //货主
@property (nonatomic, copy) NSString *ownerOrderNo;        //货主订单号
@property (nonatomic, copy) NSString *logLineId;           //日志行id
@property (nonatomic, copy) NSString *materielId;          //物料Id
@property (nonatomic, copy) NSString *materielCode;        //物料编码
@property (nonatomic, copy) NSString *materielName;        //物料名称
@property (nonatomic, copy) NSString *uom;                 //计量单位
@property (nonatomic, copy) NSString *expectQty;           //预计数量
@property (nonatomic, copy) NSString *expectNetWeight;     //预计净重
@property (nonatomic, copy) NSString *expectGrossWeight;   //预计毛重
@property (nonatomic, copy) NSString *expectGrossCubage;   //预计体积
@property (nonatomic, copy) NSString *expectPackedCount;   //预计件数
@property (nonatomic, copy) NSString *outboundQty;         //出库数量
@property (nonatomic, copy) NSString *outboundNetWeight;   //出库净重
@property (nonatomic, copy) NSString *outboundGrossWeight; //出库毛重
@property (nonatomic, copy) NSString *outboundGrossCubage; //出库体积
@property (nonatomic, copy) NSString *outboundPackedCount; //出库件数
@property (nonatomic, copy) NSString *lotNo0;              //批号0
@property (nonatomic, copy) NSString *lotNo1;              //批号1
@property (nonatomic, copy) NSString *lotNo2;              //批号2
@property (nonatomic, copy) NSString *lotNo3;              //批号3
@property (nonatomic, copy) NSString *lotNo4;              //批号4
@property (nonatomic, copy) NSString *lotNo5;              //批号5
@property (nonatomic, copy) NSString *lotNo6;              //批号6
@property (nonatomic, copy) NSString *lotNo7;              //批号7
@property (nonatomic, copy) NSString *lotNo8;              //批号8
@property (nonatomic, copy) NSString *lotNo9;              //批号9
@property (nonatomic, copy) NSString *status;              //状态(10：未入库，20：部分入库，30：全部入库，40：关闭入库，50：取消)
@property (nonatomic, copy) NSString *remarks;             //备注
@property (nonatomic, copy) NSString *gmtCreate;           //创建时间
@property (nonatomic, copy) NSString *gmtModified;         //修改时间
@property (nonatomic, copy) NSString *storeHouseId;        //收货仓库Id
@property (nonatomic, copy) NSString *storeHouseName;      //收货仓库名称
@property (nonatomic, copy) NSString *ownerName;           //货主名称
@property (nonatomic, copy) NSString *carrierId;           //供应商
@property (nonatomic, copy) NSString *carrierName;         //供应商名称
@property (nonatomic, copy) NSString *plateNumber;         //车船号
@property (nonatomic, copy) NSString *recvDate;            //预计出库时间
@property (nonatomic, copy) NSString *noticeNo;            //入库通知单号
@property (nonatomic, copy) NSString *noticeLineId;        //通知单明细键
@property (nonatomic, copy) NSString *locationId;          //储位
@property (nonatomic, copy) NSString *locationNo;          //储位号
@property (nonatomic, copy) NSString *genMethod;           //资料生成方式(10：手工创建，20：道闸触发，30：APP触发)
@property (nonatomic, copy) NSString *outboundStatus;      //出库状态
@property (nonatomic, copy) NSString *prepareStatus;       //备料状态
@property (nonatomic, copy) NSString *preparetor;          //备料员
@property (nonatomic, copy) NSString *outboundUser;        //出库员
@property (nonatomic, copy) NSString *isCanSend;           //是否发运

@end
