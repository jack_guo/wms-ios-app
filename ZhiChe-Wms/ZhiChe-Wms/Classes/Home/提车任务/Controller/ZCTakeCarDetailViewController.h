//
//  ZCTakeCarDetailViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/16.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCTakeCarDetailViewController : UIViewController
@property (nonatomic, copy) NSString *taskId;
@property (nonatomic, copy) NSString *taskType;
@property (nonatomic, assign) BOOL fromSearch;
@end
