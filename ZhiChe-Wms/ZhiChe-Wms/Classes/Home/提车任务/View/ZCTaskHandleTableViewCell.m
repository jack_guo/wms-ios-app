//
//  ZCTaskHandleTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/16.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCTaskHandleTableViewCell.h"
@interface ZCTaskHandleTableViewCell ()

@property (nonatomic, strong) UIButton *taskButton;
@property (nonatomic, strong) UIButton *qrCodeButton;
@property (nonatomic, strong) UIButton *abnormalButton;
@end

@implementation ZCTaskHandleTableViewCell
#pragma mark - 懒加载
- (UIButton *)taskButton {
    if (!_taskButton) {
        _taskButton = [[UIButton alloc] init];
        _taskButton.backgroundColor = ZCColor(0xff8213, 1);
        [_taskButton setTitleColor:ZCColor(0xffffff, 1) forState:UIControlStateNormal];
        _taskButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(36)];
        _taskButton.layer.masksToBounds = YES;
        _taskButton.layer.cornerRadius = space(6);
        [_taskButton addTarget:self action:@selector(taskButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _taskButton;
}

- (UIButton *)qrCodeButton {
    if (!_qrCodeButton) {
        _qrCodeButton = [[UIButton alloc] init];
        _qrCodeButton.backgroundColor = ZCColor(0xff6113, 1);
        [_qrCodeButton setTitle:@"赋码" forState:UIControlStateNormal];
        [_qrCodeButton setTitleColor:ZCColor(0xffffff, 1) forState:UIControlStateNormal];
        _qrCodeButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(36)];
        _qrCodeButton.layer.masksToBounds = YES;
        _qrCodeButton.layer.cornerRadius = space(6);
        [_qrCodeButton addTarget:self action:@selector(qrButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _qrCodeButton;
}

- (UIButton *)abnormalButton {
    if (!_abnormalButton) {
        _abnormalButton = [[UIButton alloc] init];
        _abnormalButton.backgroundColor = ZCColor(0xff6113, 1);
        [_abnormalButton setTitle:@"异常登记" forState:UIControlStateNormal];
        [_abnormalButton setTitleColor:ZCColor(0xffffff, 1) forState:UIControlStateNormal];
        _abnormalButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(36)];
        _abnormalButton.layer.masksToBounds = YES;
        _abnormalButton.layer.cornerRadius = space(6);
        [_abnormalButton addTarget:self action:@selector(abnormalButtonClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _abnormalButton;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.taskButton];
        [self addSubview:self.qrCodeButton];
        [self addSubview:self.abnormalButton];
    }
    return self;
}

#pragma mark - 按钮点击
- (void)taskButtonClick {
    if ([self.delegate respondsToSelector:@selector(taskHandleTableViewCellTaskButtonActionWithTaskDetailModel:)]) {
        [self.delegate taskHandleTableViewCellTaskButtonActionWithTaskDetailModel:self.taskDetailModel];
    }
}

- (void)qrButtonClick {
    if ([self.delegate respondsToSelector:@selector(taskHandleTableViewCellQRButtonActionWithTaskDetailModel:)]) {
        [self.delegate taskHandleTableViewCellQRButtonActionWithTaskDetailModel:self.taskDetailModel];
    }
}

- (void)abnormalButtonClick {
    if ([self.delegate respondsToSelector:@selector(taskHandleTableViewCellAbnormalActionWithTaskDetailModel:)]) {
        [self.delegate taskHandleTableViewCellAbnormalActionWithTaskDetailModel:self.taskDetailModel];
    }
}

#pragma mark - 属性方法
- (void)setTaskDetailModel:(ZCTaskDetailModel *)taskDetailModel {
    _taskDetailModel = taskDetailModel;
    
    __weak typeof(self)weakSelf = self;
    if ([taskDetailModel.taskType integerValue] == 10) {
        _taskButton.hidden = YES;
        [_qrCodeButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.mas_top).offset(space(40));
        }];
        if ([taskDetailModel.taskStatus integerValue] != 10 && [taskDetailModel.taskStatus integerValue] != 20) {
            _qrCodeButton.hidden = YES;
            _abnormalButton.hidden = YES;
        }
    }else if ([taskDetailModel.taskType integerValue] == 20) {
        [_taskButton setTitle:@"完成移车" forState:UIControlStateNormal];
        if ([taskDetailModel.taskStatus integerValue] != 10 && [taskDetailModel.taskStatus integerValue] != 20) {
            _qrCodeButton.hidden = YES;
            _abnormalButton.hidden = YES;
            _taskButton.enabled = NO;
            _taskButton.backgroundColor = ZCColor(0x000000, 0.34);
            [_taskButton mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(weakSelf.mas_top).offset(space(40));
            }];
        }
    }else if ([taskDetailModel.taskType integerValue] == 30) {
        [_taskButton setTitle:@"开始提车" forState:UIControlStateNormal];
        if ([taskDetailModel.taskStatus integerValue] != 10) {
            _qrCodeButton.hidden = YES;
            _abnormalButton.hidden = YES;
            _taskButton.enabled = NO;
            _taskButton.backgroundColor = ZCColor(0x000000, 0.34);
            [_taskButton mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(weakSelf.mas_top).offset(space(40));
            }];
        }
    }
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_taskButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.top.equalTo(weakSelf.mas_top).offset(space(40));
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.height.mas_equalTo(space(88));
    }];
    
    [_qrCodeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.top.equalTo(weakSelf.taskButton.mas_bottom).offset(space(60));
        make.right.equalTo(weakSelf.mas_centerX).offset(-space(20));
        make.height.mas_equalTo(space(88));
    }];
    
    [_abnormalButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.centerY.equalTo(weakSelf.qrCodeButton.mas_centerY);
        make.height.mas_equalTo(space(88));
        make.left.equalTo(weakSelf.mas_centerX).offset(space(20));
    }];
}

@end
