//
//  ZCTakeCarSearchTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCTakeCarSearchTableViewCell.h"


@interface ZCTakeCarSearchTableViewCell ()

@property (nonatomic, strong) UIImageView *originImageV;
@property (nonatomic, strong) UILabel *originLabel;
@property (nonatomic, strong) UIImageView *destImageV;
@property (nonatomic, strong) UILabel *destLabel;
@property (nonatomic, strong) UILabel *typeTitleLabel;
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UILabel *vinTitleLabel;
@property (nonatomic, strong) UILabel *vinLabel;
@property (nonatomic, strong) UILabel *statusLabel;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UIView *twoLineView;
@property (nonatomic, strong) UILabel *ownerTitleLabel;
@property (nonatomic, strong) UILabel *ownerLabel;

@end

@implementation ZCTakeCarSearchTableViewCell
#pragma mark - 懒加载
- (UIImageView *)originImageV {
    if (!_originImageV) {
        _originImageV = [[UIImageView alloc] init];
        _originImageV.image = [UIImage imageNamed:@"from"];
    }
    return _originImageV;
}

- (UILabel *)originLabel {
    if (!_originLabel) {
        _originLabel = [[UILabel alloc] init];
        _originLabel.textColor = ZCColor(0x000000, 0.87);
        _originLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _originLabel.text = @"-----";
    }
    return _originLabel;
}

- (UIImageView *)destImageV {
    if (!_destImageV) {
        _destImageV = [[UIImageView alloc] init];
        _destImageV.image = [UIImage imageNamed:@"to"];
    }
    return _destImageV;
}

- (UILabel *)destLabel {
    if (!_destLabel) {
        _destLabel = [[UILabel alloc] init];
        _destLabel.textColor = ZCColor(0x000000, 0.87);
        _destLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _destLabel.text = @"";
    }
    return _destLabel;
}

- (UILabel *)typeTitleLabel {
    if (!_typeTitleLabel) {
        _typeTitleLabel = [[UILabel alloc] init];
        _typeTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _typeTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _typeTitleLabel.text = @"类型";
    }
    return _typeTitleLabel;
}

- (UILabel *)typeLabel {
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        _typeLabel.textColor = ZCColor(0x000000, 0.87);
        _typeLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _typeLabel.text = @"----";
    }
    return _typeLabel;
}

- (UILabel *)vinTitleLabel {
    if (!_vinTitleLabel) {
        _vinTitleLabel = [[UILabel alloc] init];
        _vinTitleLabel.text = @"车架号";
        _vinTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _vinTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _vinTitleLabel;
}

- (UILabel *)vinLabel {
    if (!_vinLabel) {
        _vinLabel = [[UILabel alloc] init];
        _vinLabel.text = @"----";
        _vinLabel.textColor = ZCColor(0x000000, 0.87);
        _vinLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _vinLabel;
}

- (UILabel *)statusLabel {
    if (!_statusLabel) {
        _statusLabel = [[UILabel alloc] init];
        _statusLabel.text = @"----";
        _statusLabel.textColor = ZCColor(0x000000, 0.87);
        _statusLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _statusLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

- (UIView *)twoLineView {
    if (!_twoLineView) {
        _twoLineView = [[UIView alloc] init];
        _twoLineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _twoLineView;
}

- (UILabel *)ownerTitleLabel {
    if (!_ownerTitleLabel) {
        _ownerTitleLabel = [[UILabel alloc] init];
        _ownerTitleLabel.text = @"承运商";
        _ownerTitleLabel.textColor = ZCColor(0x000000, 0.54);
        _ownerTitleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _ownerTitleLabel;
}

- (UILabel *)ownerLabel {
    if (!_ownerLabel) {
        _ownerLabel = [[UILabel alloc] init];
        _ownerLabel.textColor = ZCColor(0x000000, 0.87);
        _ownerLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _ownerLabel.text = @"----";
    }
    return _ownerLabel;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.originImageV];
        [self addSubview:self.originLabel];
        [self addSubview:self.typeTitleLabel];
        [self addSubview:self.typeLabel];
        [self addSubview:self.destImageV];
        [self addSubview:self.destLabel];
        [self addSubview:self.ownerTitleLabel];
        [self addSubview:self.ownerLabel];
        [self addSubview:self.lineView];
        [self addSubview:self.vinTitleLabel];
        [self addSubview:self.vinLabel];
        [self addSubview:self.statusLabel];
    }
    return self;
}

#pragma mark - 属性方法
- (void)setTaskModel:(ZCTaskModel *)taskModel {
    _taskModel = taskModel;
    _originLabel.text = taskModel.originName;
    _destLabel.text = taskModel.destName;
    if ([taskModel.taskType integerValue] == 10) {
        _typeLabel.text = @"寻车任务";
    }else if ([taskModel.taskType integerValue] == 20) {
        _typeLabel.text = @"移车任务";
    }else if ([taskModel.taskType integerValue] == 30) {
        _typeLabel.text = @"提车任务";
    }else {
        _typeLabel.text = @"非任务";
    }
    
    if ([taskModel.taskStatus integerValue] == 10) {
        _statusLabel.text = @"创建";
    }else if ([taskModel.taskStatus integerValue] == 20) {
        _statusLabel.text = @"开始";
    }else if ([taskModel.taskStatus integerValue] == 30) {
        _statusLabel.text = @"完成";
    }else if ([taskModel.taskStatus integerValue] == 50) {
        _statusLabel.text = @"取消";
    }else {
        _statusLabel.text = nil;
    }
    _vinLabel.text = taskModel.vin;
    _ownerLabel.text = taskModel.serviceProviderName;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_originImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.mas_top).offset(space(40));
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.size.mas_equalTo(CGSizeMake(13, 16));
    }];
    
    [_originLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.originImageV.mas_centerY);
        make.left.equalTo(weakSelf.originImageV.mas_right).offset(space(20));
    }];
    
    [_typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.originLabel.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
    }];
    
    [_typeTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.typeLabel.mas_centerY);
        make.right.equalTo(weakSelf.typeLabel.mas_left).offset(-space(20));
    }];
    
    [_destImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.originImageV.mas_bottom).offset(space(40));
        make.left.equalTo(weakSelf.originImageV.mas_left);
        make.size.mas_equalTo(CGSizeMake(13, 16));
    }];
    
    [_destLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.destImageV.mas_centerY);
        make.left.equalTo(weakSelf.originLabel.mas_left);
        make.right.equalTo(weakSelf.mas_right).offset(-space(20));
    }];
    
    [_ownerTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.top.equalTo(weakSelf.destLabel.mas_bottom).offset(space(30));
    }];
    
    [_ownerLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.ownerTitleLabel.mas_centerY);
        make.left.equalTo(weakSelf.ownerTitleLabel.mas_right).offset(space(20));
        make.right.equalTo(weakSelf.mas_right).offset(space(40));
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.top.equalTo(weakSelf.ownerLabel.mas_bottom).offset(space(30));
        make.height.mas_equalTo(1);
    }];
    
    [_vinTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.top.equalTo(weakSelf.lineView.mas_bottom).offset(space(30));
    }];
    
    [_vinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.vinTitleLabel.mas_centerY);
        make.left.equalTo(weakSelf.vinTitleLabel.mas_right).offset(space(20));
    }];
    
    [_statusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.vinTitleLabel.mas_centerY);
        make.left.equalTo(weakSelf.typeLabel.mas_left);
    }];
    
}

@end
