//
//  ZCQualityTestingModel.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/30.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZCQualityTestingModel : NSObject

@property (nonatomic, copy) NSString *Id;                    //id
@property (nonatomic, copy) NSString *headerId;              //质检单头键
@property (nonatomic, copy) NSString *noticeLineId;          //通知单明细键
@property (nonatomic, copy) NSString *ownerId;               //货主
@property (nonatomic, copy) NSString *ownerOrderNo;          //货主订单号
@property (nonatomic, copy) NSString *materielId;            //物料Id
@property (nonatomic, copy) NSString *materielCode;          //物料代码
@property (nonatomic, copy) NSString *materielName;          //物料名称
@property (nonatomic, copy) NSString *uom;                   //计量单位
@property (nonatomic, copy) NSString *recvQty;               //收货数量
@property (nonatomic, copy) NSString *recvNetWeight;         //收货净重
@property (nonatomic, copy) NSString *recvGrossWeight;       //收货毛重
@property (nonatomic, copy) NSString *recvGrossCubage;       //收货体积
@property (nonatomic, copy) NSString *recvPackedCount;       //收货件数量
@property (nonatomic, copy) NSString *qualifiedQty;          //合格数量
@property (nonatomic, copy) NSString *qualifiedNetWeight;    //合格净重
@property (nonatomic, copy) NSString *qualifiedGrossWeight;  //合格毛重
@property (nonatomic, copy) NSString *qualifiedGrossCubage;  //合格体积
@property (nonatomic, copy) NSString *qualifiedPackedCount;  //合格件数
@property (nonatomic, copy) NSString *damagedQty;            //破损数量
@property (nonatomic, copy) NSString *damagedNetWeight;      //破损净重
@property (nonatomic, copy) NSString *damagedGrossWeight;    //破损毛重
@property (nonatomic, copy) NSString *damagedGrossCubage;    //破损体积
@property (nonatomic, copy) NSString *damagedPackedCount;    //破损件数
@property (nonatomic, copy) NSString *lotNo0;                //批号0
@property (nonatomic, copy) NSString *lotNo1;                //批号1（车架号）
@property (nonatomic, copy) NSString *lotNo2;                //批号2
@property (nonatomic, copy) NSString *lotNo3;                //批号3
@property (nonatomic, copy) NSString *lotNo4;                //批号4
@property (nonatomic, copy) NSString *lotNo5;                //批号5
@property (nonatomic, copy) NSString *lotNo6;                //批号6
@property (nonatomic, copy) NSString *lotNo7;                //批号7
@property (nonatomic, copy) NSString *lotNo8;                //批号8
@property (nonatomic, copy) NSString *lotNo9;                //批号9
@property (nonatomic, copy) NSString *status;                //状态(0：未质检，10：全部合格，20，部分合格，30：全部破损)
@property (nonatomic, copy) NSString *dealMethod;            //处理方式(10：全部入库，20：合格入库，30：拒绝入库)
@property (nonatomic, copy) NSString *remarks;               //备注
@property (nonatomic, copy) NSString *gmtCreate;             //创建时间
@property (nonatomic, copy) NSString *gmtModified;           //修改时间
@property (nonatomic, copy) NSString *storeHouseId;          //收货仓库Id
@property (nonatomic, copy) NSString *storeHouseName;        //收货仓库名称
@property (nonatomic, copy) NSString *ownerName;             //货主名称
@property (nonatomic, copy) NSString *recvDate;              //预计质检时间
@property (nonatomic, copy) NSString *taskType;              //任务类型
@property (nonatomic, copy) NSString *userCreate;            //创建人
@property (nonatomic, copy) NSString *taskNode;              //事件节点
@property (nonatomic, copy) NSString *missDescStr;           //缺件描述
@property (nonatomic, copy) NSString *exDescStr;             //缺件描述字符串
@property (nonatomic, copy) NSString *excpIds;               //异常Id集合字符串
@property (nonatomic, copy) NSString *ex_arr;                //异常数组
@property (nonatomic, copy) NSString *miss_arr;              //缺件数组
@property (nonatomic, copy) NSString *picInfo;               //图片URL
@property (nonatomic, copy) NSString *isCanSend;             //是否发运

@end
