//
//  ZCLoadingDetailViewController.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/7.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZCLoadingDetailViewController : UIViewController

@property (nonatomic, copy) NSString *keyId;
@property (nonatomic, copy) NSString *visitType;

@end
