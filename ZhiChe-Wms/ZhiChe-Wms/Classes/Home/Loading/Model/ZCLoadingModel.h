//
//  ZCLoadingModel.h
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/7.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZCLoadingInfoModel : NSObject

@property (nonatomic, copy) NSString *Id;                       //Id
@property (nonatomic, copy) NSString *releaseGid;               //系统单号
@property (nonatomic, copy) NSString *shipmentGid;              //调度指令Id
@property (nonatomic, copy) NSString *customerId;               //客户Id
@property (nonatomic, copy) NSString *cusOrderNo;               //客户订单号
@property (nonatomic, copy) NSString *cusWaybillNo;             //客户运单号
@property (nonatomic, copy) NSString *cusTransMode;             //客户运输模式分类
@property (nonatomic, copy) NSString *isUrgent;                 //是否急发
@property (nonatomic, copy) NSString *pickupIsAppt;             //是否超期
@property (nonatomic, copy) NSString *orderAtt;                 //订单属性
@property (nonatomic, copy) NSString *expectInboundDate;        //要求入库时间
@property (nonatomic, copy) NSString *expectReceiptDate;        //要求回单时间
@property (nonatomic, copy) NSString *expcetShipDate;           //要求发运时间
@property (nonatomic, copy) NSString *expectArriveDate;         //要求抵达时间
@property (nonatomic, copy) NSString *logLineId;                //日志行Id
@property (nonatomic, copy) NSString *sourceKey;                //来源唯一键
@property (nonatomic, copy) NSString *sourceNo;                 //来源单据号
@property (nonatomic, copy) NSString *originLocationGid;        //起运地编码
@property (nonatomic, copy) NSString *outboundHouseId;          //出库仓库Id
@property (nonatomic, copy) NSString *originLocationName;       //起运地名称
@property (nonatomic, copy) NSString *originLocationProvince;   //起运地省份
@property (nonatomic, copy) NSString *originLocationCity;       //起运地城市
@property (nonatomic, copy) NSString *originLocationCounty;     //起运地区县
@property (nonatomic, copy) NSString *originLocationAddress;    //起运地地址
@property (nonatomic, copy) NSString *destLocationGid;          //目的地编码
@property (nonatomic, copy) NSString *inboundHouseId;           //入库仓库Id
@property (nonatomic, copy) NSString *destLocationName;         //目的地名称
@property (nonatomic, copy) NSString *destLocationProvince;     //目的地省份
@property (nonatomic, copy) NSString *destLocationCity;         //目的地城市
@property (nonatomic, copy) NSString *destLocationCounty;       //目的地区县
@property (nonatomic, copy) NSString *destLocationAddress;      //目的地地址
@property (nonatomic, copy) NSString *cusVehicleType;           //客户车型
@property (nonatomic, copy) NSString *vehicleDescribe;          //车型描述
@property (nonatomic, copy) NSString *stanVehicleType;          //标准车型
@property (nonatomic, copy) NSString *vin;                      //VIN码、车架号
@property (nonatomic, copy) NSString *isModVehicle;             //是否改装车
@property (nonatomic, copy) NSString *modVehicleSize;           //改装后的长宽高重
@property (nonatomic, copy) NSString *qrCode;                   //二维码
@property (nonatomic, copy) NSString *remarks;                  //备注
@property (nonatomic, copy) NSString *status;                   //状态(10：正常，50：取消)
@property (nonatomic, copy) NSString *gmtModified;              //修改时间
@property (nonatomic, copy) NSString *gmtCreate;                //创建时间
@property (nonatomic, copy) NSString *isCanSend;                //是否发运

@end

@interface ZCLoadingConditionModel : NSObject

@property (nonatomic, copy) NSString *key;

@end

@interface ZCLoadingModel : NSObject

@property (nonatomic, copy) NSString *offset;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *size;
@property (nonatomic, copy) NSString *pages;
@property (nonatomic, copy) NSString *current;
@property (nonatomic, copy) NSString *searchCount;
@property (nonatomic, copy) NSString *openSort;
@property (nonatomic, copy) NSString *orderByField;


@property (nonatomic, strong) NSArray<ZCLoadingInfoModel *> *records;
@property (nonatomic, strong) ZCLoadingConditionModel *condition;
@property (nonatomic, copy) NSString *offsetCurrent;
@property (nonatomic, copy) NSString *asc;

@end
