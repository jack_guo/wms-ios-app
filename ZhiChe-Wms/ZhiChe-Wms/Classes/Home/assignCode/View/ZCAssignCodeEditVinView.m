//
//  ZCAssignCodeEditVinView.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAssignCodeEditVinView.h"

@interface ZCAssignCodeEditVinView ()

@property (nonatomic, strong) UILabel *vinLabel;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic, strong) UITextField *vinTextField;
@property (nonatomic, strong) UIButton *scanButton;
@property (nonatomic, strong) UIButton *commitButton;

@end

@implementation ZCAssignCodeEditVinView
#pragma mark - 懒加载
- (UILabel *)vinLabel {
    if (!_vinLabel) {
        _vinLabel = [[UILabel alloc] init];
        _vinLabel.textColor = ZCColor(0x000000, 0.54);
        _vinLabel.text = @"车架号";
        _vinLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _vinLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

- (UITextField *)vinTextField {
    if (!_vinTextField) {
        _vinTextField = [[UITextField alloc] init];
        _vinTextField.textColor = ZCColor(0x000000, 0.87);
        _vinTextField.font = [UIFont systemFontOfSize:FontSize(28)];
        _vinTextField.keyboardType = UIKeyboardTypeASCIICapable;
        [_vinTextField addTarget:self action:@selector(editVin:) forControlEvents:UIControlEventEditingChanged];
    }
    return _vinTextField;
}

- (UIButton *)scanButton {
    if (!_scanButton) {
        _scanButton = [[UIButton alloc] init];
        [_scanButton setImage:[UIImage imageNamed:@"scanQR"] forState:UIControlStateNormal];
        [_scanButton addTarget:self action:@selector(scanButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _scanButton;
}

- (UIButton *)commitButton {
    if (!_commitButton) {
        _commitButton = [[UIButton alloc] init];
        [_commitButton setTitle:@"确定" forState:UIControlStateNormal];
        [_commitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_commitButton setBackgroundColor:ZCColor(0xff8213, 0.6)];
        _commitButton.layer.masksToBounds = YES;
        _commitButton.layer.cornerRadius = space(6);
        _commitButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        [_commitButton addTarget:self action:@selector(commitButtonClcik:) forControlEvents:UIControlEventTouchUpInside];
        _commitButton.enabled = NO;
    }
    return _commitButton;
}

#pragma mark - 初始化
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addSubview:self.vinLabel];
        [self addSubview:self.lineView];
        [self addSubview:self.vinTextField];
        [self addSubview:self.scanButton];
        [self addSubview:self.commitButton];
        [self.vinTextField becomeFirstResponder];
    }
    return self;
}

#pragma mark - 按钮点击
- (void)scanButtonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(assignCodeEditVinViewScanButtonClick)]) {
        [self.delegate assignCodeEditVinViewScanButtonClick];
    }
}

- (void)commitButtonClcik:(UIButton *)sender {
    [self.vinTextField resignFirstResponder];
    if ([self.delegate respondsToSelector:@selector(assignCodeEditVinViewCommitButtonClick)]) {
        [self.delegate assignCodeEditVinViewCommitButtonClick];
    }
}

- (void)editVin:(UITextField *)textfield {
    if (textfield.text.length > 0) {
        [self.commitButton setBackgroundColor:ZCColor(0xff8213, 1)];
        self.commitButton.enabled = YES;
    }else {
        [self.commitButton setBackgroundColor:ZCColor(0xff8213, 0.6)];
        self.commitButton.enabled = NO;
    }
    if ([self.delegate respondsToSelector:@selector(assignCodeEditVinViewEditVin:)]) {
        [self.delegate assignCodeEditVinViewEditVin:textfield.text];
    }
}

#pragma mark - 属性方法
- (void)setVin:(NSString *)vin {
    _vin = vin;
    self.vinTextField.text = vin;
    if (vin.length > 0) {
        [self.commitButton setBackgroundColor:ZCColor(0xff8213, 1)];
        self.commitButton.enabled = YES;
    }else {
        [self.commitButton setBackgroundColor:ZCColor(0xff8213, 0.6)];
        self.commitButton.enabled = NO;
    }
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.vinLabel.text = title;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_vinLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.top.equalTo(weakSelf.mas_top).offset(space(80));
        make.width.mas_equalTo(space(140));
    }];
    
    [_vinTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.vinLabel.mas_right).offset(space(20));
        make.centerY.equalTo(weakSelf.vinLabel.mas_centerY);
        make.right.equalTo(weakSelf.scanButton.mas_left).offset(-space(20));
    }];
    
    [_scanButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.vinTextField.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.size.mas_equalTo(CGSizeMake(50, 50));
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.vinTextField.mas_left);
        make.right.equalTo(weakSelf.vinTextField.mas_right);
        make.top.equalTo(weakSelf.vinTextField.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    
    [_commitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(80));
        make.right.equalTo(weakSelf.mas_right).offset(-space(80));
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(space(90));
    }];
}
@end
