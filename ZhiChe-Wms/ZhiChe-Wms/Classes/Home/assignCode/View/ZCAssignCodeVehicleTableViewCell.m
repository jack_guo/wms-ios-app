//
//  ZCAssignCodeVehicleTableViewCell.m
//  ZhiChe-Wms
//
//  Created by grj on 2018/12/12.
//  Copyright © 2018 Regina. All rights reserved.
//

#import "ZCAssignCodeVehicleTableViewCell.h"

@interface ZCAssignCodeVehicleTableViewCell ()
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIView *lineView;
@end

@implementation ZCAssignCodeVehicleTableViewCell
#pragma mark - 懒加载
- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.font = [UIFont systemFontOfSize:FontSize(26)];
        _contentLabel.textColor = ZCColor(0x000000, 0.87);
    }
    return _contentLabel;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _lineView;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.contentLabel];
        [self addSubview:self.lineView];
    }
    return self;
}

#pragma mark - 属性设置
- (void)setContent:(NSString *)content {
    _content = content;
    _contentLabel.text = content;
}

- (void)setShowLine:(BOOL)showLine {
    _showLine = showLine;
    _lineView.hidden = showLine ? NO : YES;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(20));
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(1);
    }];
}

@end
