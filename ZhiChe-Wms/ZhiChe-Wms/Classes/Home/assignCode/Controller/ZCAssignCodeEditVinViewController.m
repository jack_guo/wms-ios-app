//
//  ZCAssignCodeEditVinViewController.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/19.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAssignCodeEditVinViewController.h"
#import "ZCAssignCodeEditVinView.h"
#import "UWRQViewController.h"

@interface ZCAssignCodeEditVinViewController ()<ZCAssignCodeEditVinViewDelegate,uwRQDelegate>

@property (nonatomic, strong) ZCAssignCodeEditVinView *editVinView;
@property (nonatomic, copy) NSString *vin;
@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation ZCAssignCodeEditVinViewController

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [backButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    self.editVinView = [[ZCAssignCodeEditVinView alloc] initWithFrame:CGRectMake(0, 64, SCREENWIDTH, space(400))];
    self.editVinView.delegate = self;
    if ([self.name isEqualToString:@"vin"]) {
        self.navigationItem.title = @"车架号录入";
        self.editVinView.title = @"车架号";
    }else {
        self.navigationItem.title = @"实车确认";
        self.editVinView.title = @"实车位置";
    }
    [self.view addSubview:self.editVinView];
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hud];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.hud hideAnimated:YES];
    self.hud.mode = MBProgressHUDModeIndeterminate;
}

#pragma mark - 按钮点击
- (void)backButtonClick:(UIButton *)sender {
    if (self.bindCodeBlock) {
        self.bindCodeBlock(NO);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - uwQRDelegate
- (void)uwRQFinshedScan:(NSString *)result {
    NSArray *arr = [result componentsSeparatedByString:@","];
    self.vin = [arr firstObject];
    self.editVinView.vin = self.vin;
}

#pragma mark - ZCAssignCodeEditVinViewDelegate
- (void)assignCodeEditVinViewEditVin:(NSString *)vin {
    self.vin = vin;
}

- (void)assignCodeEditVinViewScanButtonClick {
    UWRQViewController *qrVC = [[UWRQViewController alloc] init];
    qrVC.delegate = self;
    [self.navigationController pushViewController:qrVC animated:YES];
}

- (void)assignCodeEditVinViewCommitButtonClick {
    if ([self.name isEqualToString:@"vin"]) {
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setObject:self.key forKey:@"condition[key]"];
        [param setObject:self.vin forKey:@"condition[vin]"];
        
        [self.hud showAnimated:YES];
        __weak typeof(self)weakSelf = self;
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [ZCHttpTool postWithURL:EditVin params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if ([responseObject[@"code"] integerValue] == 0) {
                    [weakSelf.hud hideAnimated:YES];
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }else {
                    weakSelf.hud.mode = MBProgressHUDModeText;
                    weakSelf.hud.label.text = responseObject[@"message"];
                    [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
                }
            } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = error.localizedDescription;
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
            }];
        });
    }else {
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setObject:self.key forKey:@"condition[lotNo1]"];
        NSString *houseId = [[NSUserDefaults standardUserDefaults] objectForKey:UserStoreId];
        [param setObject:houseId forKeyedSubscript:@"condition[houseId]"];
        [param setObject:self.vin forKey:@"condition[storeDetail]"];
        
        [self.hud showAnimated:YES];
        __weak typeof(self)weakSelf = self;
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [ZCHttpTool postWithURL:BindStoreDetail params:param success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                if ([responseObject[@"code"] integerValue] == 0) {
                    [weakSelf.hud hideAnimated:YES];
                    if (weakSelf.bindCodeBlock) {
                        weakSelf.bindCodeBlock(YES);
                    }
                    [weakSelf.navigationController popViewControllerAnimated:YES];
                }else {
                    weakSelf.hud.mode = MBProgressHUDModeText;
                    weakSelf.hud.label.text = responseObject[@"message"];
                    [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
                    if (weakSelf.bindCodeBlock) {
                        weakSelf.bindCodeBlock(NO);
                    }
                }
            } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
                weakSelf.hud.mode = MBProgressHUDModeText;
                weakSelf.hud.label.text = error.localizedDescription;
                [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
                if (weakSelf.bindCodeBlock) {
                    weakSelf.bindCodeBlock(NO);
                }
            }];
        });
    }
   
}


@end
