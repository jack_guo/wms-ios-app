//
//  ZCAssignCodeModel.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/20.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCAssignCodeModel.h"

@implementation ZCAssignCodeModel

@end

@implementation ZCAssignCodeDetailModel

@end

@implementation ZCAssignCodeVehicleInfoModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"vehicleInfoId" : @"id"};
}

@end

@implementation ZCAssignCodeVehicleModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"records" : [ZCAssignCodeVehicleInfoModel class]};
}

@end
