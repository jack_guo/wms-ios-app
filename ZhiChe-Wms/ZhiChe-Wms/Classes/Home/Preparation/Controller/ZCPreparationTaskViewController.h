//
//  ZCPreparationTaskViewController.h
//  ZhiChe-Wms
//
//  Created by grj on 2018/12/17.
//  Copyright © 2018 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZCPreparationTaskViewController : UIViewController

@property (nonatomic, assign) BOOL isCheck;
@property (nonatomic, copy) NSString *taskInfoId;

@end

NS_ASSUME_NONNULL_END
