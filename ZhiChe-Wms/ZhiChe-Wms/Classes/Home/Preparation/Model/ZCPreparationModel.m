//
//  ZCPreparationModel.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/6.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCPreparationModel.h"

@implementation ZCPreparationModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"Id" : @"id"};
}

@end

@implementation ZCPreparationTaskInfoModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"taskInfoId" : @"id"};
}

@end

@implementation ZCPreparationListModel

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"records" : [ZCPreparationTaskInfoModel class]};
}

@end
