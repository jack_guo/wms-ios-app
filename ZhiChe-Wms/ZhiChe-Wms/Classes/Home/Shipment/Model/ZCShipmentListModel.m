//
//  ZCShipmentListModel.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/9/10.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCShipmentListModel.h"


@implementation ZCShipmentOrderModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"Id" : @"id"};
}

@end

@implementation ZCShipmentModel
+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"Id" : @"id"};
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"otmOrderReleaseList" : [ZCShipmentOrderModel class]};
}

@end

@implementation ZCShipmentConditionModel

@end

@implementation ZCShipmentListModel
+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"records" : [ZCShipmentModel class]};
}

@end
