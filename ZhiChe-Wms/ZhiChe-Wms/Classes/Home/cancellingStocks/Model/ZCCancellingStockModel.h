//
//  ZCCancellingStockModel.h
//  ZhiChe-Wms
//
//  Created by grj on 2018/12/21.
//  Copyright © 2018 Regina. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZCCancellingStockDetailModel : NSObject
@property (nonatomic, copy) NSString *cancelId;               //主键id
@property (nonatomic, copy) NSString *noticeNo;               //通知单号
@property (nonatomic, copy) NSString *ownerOrderNo;           //订单号
@property (nonatomic, copy) NSString *ownerId;                //客户
@property (nonatomic, copy) NSString *houseName;              //仓库
@property (nonatomic, copy) NSString *vin;                    //车架号
@property (nonatomic, copy) NSString *stanVehicleType;        //车型
@property (nonatomic, copy) NSString *vehicleDescribe;        //车型描述
@property (nonatomic, copy) NSString *prepareDriveWay;        //备车道
@property (nonatomic, copy) NSString *storeHouseId;           //仓库ID
@property (nonatomic, copy) NSString *outboundLineId;         //出库lineId
@property (nonatomic, copy) NSString *userCancel;             //取消人
@property (nonatomic, copy) NSString *userReceive;            //领取人
@property (nonatomic, copy) NSString *gmtCreate;              //创建时间
@property (nonatomic, copy) NSString *gmtModified;            //修改时间
@property (nonatomic, copy) NSString *statusOutbound;         //出库状态
@property (nonatomic, copy) NSString *locationName;           //库区
@property (nonatomic, copy) NSString *areaName;               //库位
@property (nonatomic, copy) NSString *statusReceive;          //任务状态
@property (nonatomic, copy) NSString *key;                    //id或二维码
@property (nonatomic, copy) NSString *visitType;              //访问方式
@property (nonatomic, copy) NSString *remark;                 //退库原因
@end

@interface ZCCancellingStockConditionModel : NSObject
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSString *storeHouseId;

@end

@interface ZCCancellingStockModel : NSObject

@property (nonatomic, copy) NSString *offset;
@property (nonatomic, copy) NSString *limit;
@property (nonatomic, copy) NSString *total;
@property (nonatomic, copy) NSString *size;
@property (nonatomic, copy) NSString *pages;
@property (nonatomic, copy) NSString *current;
@property (nonatomic, copy) NSString *searchCount;
@property (nonatomic, copy) NSString *openSort;
@property (nonatomic, copy) NSString *orderByField;
@property (nonatomic, strong) NSArray<ZCCancellingStockDetailModel *> *records;
@property (nonatomic, strong) ZCCancellingStockConditionModel *condition;
@property (nonatomic, copy) NSString *offsetCurrent;
@property (nonatomic, copy) NSString *asc;

@end


NS_ASSUME_NONNULL_END
