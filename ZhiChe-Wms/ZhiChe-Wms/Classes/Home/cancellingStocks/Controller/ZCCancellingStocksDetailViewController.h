//
//  ZCCancellingStocksDetailViewController.h
//  ZhiChe-Wms
//
//  Created by grj on 2018/12/21.
//  Copyright © 2018 Regina. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZCCancellingStocksDetailViewController : UIViewController

@property (nonatomic, copy) NSString *keyId;
@property (nonatomic, copy) NSString *visitType;

@end

NS_ASSUME_NONNULL_END
