//
//  ZCCancellingStocksDetailViewController.m
//  ZhiChe-Wms
//
//  Created by grj on 2018/12/21.
//  Copyright © 2018 Regina. All rights reserved.
//

#import "ZCCancellingStocksDetailViewController.h"
#import "ZCTaskTotalNumTableViewCell.h"
#import "ZCCancellingStockModel.h"
#import "ZCCancellingHandleTableViewCell.h"

@interface ZCCancellingStocksDetailViewController ()<UITableViewDelegate, UITableViewDataSource, ZCCancellingHandleTableViewCellDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ZCCancellingStockDetailModel *detailModel;
@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, assign) BOOL isHandle;

@end

static NSString *cancellingDetailCellID = @"ZCTaskTotalNumTableViewCell";
static NSString *cancellingHandleCellID = @"ZCCancellingHandleTableViewCell";

@implementation ZCCancellingStocksDetailViewController
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH, SCREENHEIGHT) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
            _tableView.scrollIndicatorInsets = _tableView.contentInset;
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
        }
        
        [_tableView registerClass:[ZCTaskTotalNumTableViewCell class] forCellReuseIdentifier:cancellingDetailCellID];
        [_tableView registerClass:[ZCCancellingHandleTableViewCell class] forCellReuseIdentifier:cancellingHandleCellID];
    }
    return _tableView;
}

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"退库任务详情";
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    [backButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backToForward) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    [self.view addSubview:self.tableView];
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hud];
    [self.hud showAnimated:YES];
    [self loadData];
}

#pragma mark - 按钮点击
- (void)backToForward {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 网络请求
- (void)loadData {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.keyId forKey:@"key"];
    [params setObject:self.visitType forKey:@"visitType"];
    if ([self.visitType isEqualToString:@"SCAN"]) {
        NSString *houserId = [[NSUserDefaults standardUserDefaults] objectForKey:UserStoreId];
        [params setObject:houserId forKey:@"storeHouseId"];
    }
    __weak typeof(self)weakSelf = self;
    [self.hud showAnimated:YES];
    [ZCHttpTool postWithURL:CancellingStockDetail params:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == 0) {
            [weakSelf.hud hideAnimated:YES];
            weakSelf.detailModel = [ZCCancellingStockDetailModel yy_modelWithJSON:responseObject[@"data"]];
            weakSelf.isHandle = [weakSelf.detailModel.statusReceive integerValue] == 10 ? YES : NO;
            [weakSelf.tableView reloadData];
        }else {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = responseObject[@"message"];
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        weakSelf.hud.mode = MBProgressHUDModeText;
        weakSelf.hud.label.text = error.localizedDescription;
        [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isHandle) {
        return 11;
    }else {
        return 10;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 10) {
        ZCCancellingHandleTableViewCell *handleCell = [tableView dequeueReusableCellWithIdentifier:cancellingHandleCellID forIndexPath:indexPath];
        handleCell.delegate = self;
        
        return handleCell;
    }else {
        ZCTaskTotalNumTableViewCell *contentCell = [tableView dequeueReusableCellWithIdentifier:cancellingDetailCellID forIndexPath:indexPath];
        contentCell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 0) {
            contentCell.title = @"通知单号";
            contentCell.content = self.detailModel.noticeNo;
        }else if (indexPath.row == 1) {
            contentCell.title = @"订单号";
            contentCell.content = self.detailModel.ownerOrderNo;
        }else if (indexPath.row == 2) {
            contentCell.title = @"车架号";
            contentCell.content = self.detailModel.vin;
        }else if (indexPath.row == 3) {
            contentCell.title = @"车型";
            contentCell.content = self.detailModel.stanVehicleType;
        }else if (indexPath.row == 4) {
            contentCell.title = @"出库状态";
            contentCell.content = self.detailModel.statusOutbound;
        }else if (indexPath.row == 5) {
            contentCell.title = @"仓库";
            contentCell.content = self.detailModel.houseName;
        }else if (indexPath.row == 6) {
            contentCell.title = @"库区";
            contentCell.content = self.detailModel.locationName;
        }else if (indexPath.row == 7) {
            contentCell.title = @"库位";
            contentCell.content = self.detailModel.areaName;
        }else if (indexPath.row == 8) {
            contentCell.title = @"任务状态";
            if ([self.detailModel.statusReceive integerValue] == 10) {
                contentCell.content = @"未领取";
            }else if ([self.detailModel.statusReceive integerValue] == 20) {
                contentCell.content = @"已领取";
            }else if ([self.detailModel.statusReceive integerValue] == 30) {
                contentCell.content = @"已完成";
            }
            contentCell.contentColor = ZCColor(0xff8213, 1);
        }else if (indexPath.row == 9) {
            contentCell.title = @"退库原因";
            contentCell.content = self.detailModel.remark;
        }
        return contentCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 10) {
        return space(150);
    }else {
        return space(90);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

#pragma mark - ZCCancellingHandleTableViewCellDelegate
//领取退库任务
- (void)cancellingHandleTableViewCellTakeTaskAction:(UIButton *)sender {
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.detailModel.cancelId forKey:@"condition[keyId]"];
    __weak typeof(self)weakSelf = self;
    [ZCHttpTool postWithURL:PickUpCancelTask params:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject[@"code"] integerValue] == 0) {
            [weakSelf loadData];
        }else {
            weakSelf.hud.mode = MBProgressHUDModeText;
            weakSelf.hud.label.text = responseObject[@"message"];
            [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
        }
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        weakSelf.hud.mode = MBProgressHUDModeText;
        weakSelf.hud.label.text = error.localizedDescription;
        [weakSelf.hud hideAnimated:YES afterDelay:HudTime];
    }];
    
}
@end
