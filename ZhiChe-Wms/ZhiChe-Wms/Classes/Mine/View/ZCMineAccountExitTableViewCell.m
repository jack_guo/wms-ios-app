//
//  ZCMineAccountExitTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/29.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCMineAccountExitTableViewCell.h"


@interface ZCMineAccountExitTableViewCell ()

@property (nonatomic, strong) UIButton *exitButton;


@end

@implementation ZCMineAccountExitTableViewCell
#pragma mark - 懒加载
- (UIButton *)exitButton {
    if (!_exitButton) {
        _exitButton = [[UIButton alloc] init];
        [_exitButton setTitle:@"退出登录" forState:UIControlStateNormal];
        [_exitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_exitButton setBackgroundColor:ZCColor(0xff8213, 1)];
        _exitButton.titleLabel.font = [UIFont systemFontOfSize:FontSize(32)];
        _exitButton.layer.masksToBounds = YES;
        _exitButton.layer.cornerRadius = space(6);
        [_exitButton addTarget:self action:@selector(exitButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _exitButton;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.exitButton];
    }
    return self;
}

#pragma mark - 按钮点击
- (void)exitButtonClick:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(mineAccountExitTableViewCellExitButton:)]) {
        [self.delegate mineAccountExitTableViewCellExitButton:sender];
    }
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_exitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(100));
        make.right.equalTo(weakSelf.mas_right).offset(-space(100));
        make.top.equalTo(weakSelf.mas_top).offset(space(80));
        make.height.mas_equalTo(space(90));
    }];
}

@end
