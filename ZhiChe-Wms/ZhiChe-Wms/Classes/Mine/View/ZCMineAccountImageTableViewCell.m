//
//  ZCMineAccountImageTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/28.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCMineAccountImageTableViewCell.h"

@interface ZCMineAccountImageTableViewCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *headImageView;
@property (nonatomic, strong) UIView *line;
@end

@implementation ZCMineAccountImageTableViewCell
#pragma mark - 懒加载
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = ZCColor(0x000000, 0.54);
        _titleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
        _titleLabel.text = @"头像";
    }
    return _titleLabel;
}

- (UIImageView *)headImageView {
    if (!_headImageView) {
        _headImageView = [[UIImageView alloc] init];
        _headImageView.image = [UIImage imageNamed:@"headImage"];
    }
    return _headImageView;
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _line;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.titleLabel];
        [self addSubview:self.headImageView];
        [self addSubview:self.line];
    }
    return self;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [_headImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.size.mas_equalTo(CGSizeMake(space(120), space(120)));
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left);
        make.right.equalTo(weakSelf.mas_right);
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(1);
    }];
}


@end
