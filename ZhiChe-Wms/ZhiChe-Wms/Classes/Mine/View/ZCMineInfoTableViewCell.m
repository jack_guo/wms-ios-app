//
//  ZCMineInfoTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/29.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCMineInfoTableViewCell.h"

@interface ZCMineInfoTableViewCell ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UIView *line;

@end

@implementation ZCMineInfoTableViewCell
#pragma mark - 懒加载
- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = ZCColor(0x000000, 0.54);
        _titleLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _titleLabel;
}

- (UILabel *)descLabel {
    if (!_descLabel) {
        _descLabel = [[UILabel alloc] init];
        _descLabel.textColor = ZCColor(0x000000, 0.87);
        _descLabel.font = [UIFont systemFontOfSize:FontSize(28)];
    }
    return _descLabel;
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _line;
}

#pragma mark - 属性方法
- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = title;
}

- (void)setDesc:(NSString *)desc {
    _desc = desc;
    self.descLabel.text = desc;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self addSubview:self.titleLabel];
        [self addSubview:self.descLabel];
        [self addSubview:self.line];
    }
    return self;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [_descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.centerY.equalTo(weakSelf.mas_centerY);
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(1);
    }];
}


@end
