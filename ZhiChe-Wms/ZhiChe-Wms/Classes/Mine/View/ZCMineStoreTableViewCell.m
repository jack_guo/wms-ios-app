//
//  ZCMineStoreTableViewCell.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/28.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCMineStoreTableViewCell.h"

@interface ZCMineStoreTableViewCell ()

@property (nonatomic, strong) UILabel *storeLabel;
@property (nonatomic, strong) UIView *line;

@end

@implementation ZCMineStoreTableViewCell
#pragma mark - 懒加载
- (UILabel *)storeLabel {
    if (!_storeLabel) {
        _storeLabel = [[UILabel alloc] init];
        _storeLabel.textColor = ZCColor(0x000000, 0.87);
        _storeLabel.font = [UIFont systemFontOfSize:FontSize(26)];
    }
    return _storeLabel;
}

- (UIView *)line {
    if (!_line) {
        _line = [[UIView alloc] init];
        _line.backgroundColor = ZCColor(0x000000, 0.05);
    }
    return _line;
}

#pragma mark - 初始化
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.storeLabel];
        [self addSubview:self.line];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    self.storeLabel.textColor = selected ? ZCColor(0xff8213, 1) : ZCColor(0x000000, 0.87);
}

#pragma mark - 属性方法
- (void)setStore:(NSString *)store {
    _store = store;
    self.storeLabel.text = store;
}

#pragma mark - 布局
+ (BOOL)requiresConstraintBasedLayout {
    return YES;
}

- (void)updateConstraints {
    [super updateConstraints];
    __weak typeof(self)weakSelf = self;
    [_storeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.mas_centerY);
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
    }];
    
    [_line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(weakSelf.mas_left).offset(space(40));
        make.right.equalTo(weakSelf.mas_right).offset(-space(40));
        make.bottom.equalTo(weakSelf.mas_bottom);
        make.height.mas_equalTo(1);
    }];
}

@end
