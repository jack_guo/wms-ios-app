//
//  ZCMineAccountViewController.m
//  ZhiChe-Wms
//
//  Created by 高睿婕 on 2018/8/28.
//  Copyright © 2018年 Regina. All rights reserved.
//

#import "ZCMineAccountViewController.h"
#import "ZCMineAccountImageTableViewCell.h"
#import "ZCMineInfoTableViewCell.h"
#import "ZCMineAccountExitTableViewCell.h"
#import "ZCLoginViewController.h"
#import "ZCMineNullView.h"


static NSString *imageCellID = @"ZCMineAccountImageTableViewCell";
static NSString *infoCellID = @"ZCMineInfoTableViewCell";
static NSString *exitCellID = @"ZCMineAccountExitTableViewCell";

@interface ZCMineAccountViewController ()<UITableViewDelegate, UITableViewDataSource, ZCMineAccountExitTableViewCellDelegate>
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation ZCMineAccountViewController
#pragma mark - 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH, SCREENHEIGHT) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.showsHorizontalScrollIndicator = NO;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.backgroundColor = ZCColor(0xffffff, 1);
        _tableView.scrollEnabled = NO;
        if (@available(iOS 11.0, *)) {
            _tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
            _tableView.contentInset = UIEdgeInsetsMake(64, 0, 0, 0);
            _tableView.scrollIndicatorInsets = _tableView.contentInset;
            _tableView.estimatedRowHeight = 0;
            _tableView.estimatedSectionFooterHeight = 0;
            _tableView.estimatedSectionHeaderHeight = 0;
        }
        [_tableView registerClass:[ZCMineAccountImageTableViewCell class] forCellReuseIdentifier:imageCellID];
        [_tableView registerClass:[ZCMineInfoTableViewCell class] forCellReuseIdentifier:infoCellID];
        [_tableView registerClass:[ZCMineAccountExitTableViewCell class] forCellReuseIdentifier:exitCellID];
    }
    return _tableView;
}

#pragma mark - 生命周期
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBarController.tabBar.hidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, space(30), space(30))];
    [leftButton setImage:[UIImage imageNamed:@"返回"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(leftButtonToBack) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    self.navigationItem.title = @"账号管理";
    [self.view addSubview:self.tableView];
}

#pragma mark - 按钮点击
- (void)leftButtonToBack {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        ZCMineAccountImageTableViewCell *imageCell = [tableView dequeueReusableCellWithIdentifier:imageCellID forIndexPath:indexPath];
        imageCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return imageCell;
    }else if (indexPath.row == 5) {
        ZCMineAccountExitTableViewCell *exitCell = [tableView dequeueReusableCellWithIdentifier:exitCellID forIndexPath:indexPath];
        exitCell.selectionStyle = UITableViewCellSelectionStyleNone;
        exitCell.delegate = self;
        return exitCell;
    }else {
        ZCMineInfoTableViewCell *infoCell = [tableView dequeueReusableCellWithIdentifier:infoCellID forIndexPath:indexPath];
        infoCell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 1) {
            infoCell.title = @"姓名";
            infoCell.desc = [[NSUserDefaults standardUserDefaults] objectForKey:UserName];
        }else if (indexPath.row == 2) {
            infoCell.title = @"电话";
            infoCell.desc = [[NSUserDefaults standardUserDefaults] objectForKey:UserMobile];
        }else if (indexPath.row == 3) {
            infoCell.title = @"邮箱";
            infoCell.desc = [[NSUserDefaults standardUserDefaults] objectForKey:UserEmail];
        }else if (indexPath.row == 4) {
            infoCell.title = @"公司";
            
        }
        return infoCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return space(200);
    }else if (indexPath.row == 5) {
        return space(300);
    }
    return space(90);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return space(20);
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] init];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[ZCMineNullView alloc] init];
}

#pragma mark - ZCMineAccountExitTableViewCellDelegate
- (void)mineAccountExitTableViewCellExitButton:(UIButton *)button {
    ZCLoginViewController *loginVC = [[ZCLoginViewController alloc] init];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    window.rootViewController = loginVC;
}


@end
